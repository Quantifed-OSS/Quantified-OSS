<?php
namespace App\Test\Fixture;

use Cake\I18n\Time;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * MarksFixture
 *
 */
class MarksFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'repository_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'raw_final_value_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'm_last_commit_age' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'm_project_age' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'm_delta_commit_1m' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'm_delta_commit_12m' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'm_nb_contributors' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'm_highest_committer_percent' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'raw_final_value_id_unique' => ['type' => 'unique', 'columns' => ['raw_final_value_id'], 'length' => []],
            'repository_mark_fk' => ['type' => 'foreign', 'columns' => ['repository_id'], 'references' => ['repositories', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
            'rfv_mark_fk' => ['type' => 'foreign', 'columns' => ['raw_final_value_id'], 'references' => ['raw_final_values', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public function init()
    {
        $this->records = [
            [
                'created' => new Time('2 days ago'),
                'repository_id' => 1,
                'raw_final_value_id' => 1,
                'm_last_commit_age' => 1,
                'm_project_age' => 1,
                'm_delta_commit_1m' => 1,
                'm_delta_commit_12m' => 1,
                'm_nb_contributors' => 1,
                'm_highest_committer_percent' => 1
            ],
            [
                'created' => new Time('40 days ago'),
                'repository_id' => 1,
                'raw_final_value_id' => 2,
                'm_last_commit_age' => 1,
                'm_project_age' => 1,
                'm_delta_commit_1m' => 1,
                'm_delta_commit_12m' => 1,
                'm_nb_contributors' => 1,
                'm_highest_committer_percent' => 1
            ],
            [
                'created' => new Time('10 days ago'),
                'repository_id' => 1,
                'raw_final_value_id' => 3,
                'm_last_commit_age' => 1,
                'm_project_age' => 1,
                'm_delta_commit_1m' => 1,
                'm_delta_commit_12m' => 1,
                'm_nb_contributors' => 1,
                'm_highest_committer_percent' => 1
            ],
        ];
        parent::init();
    }

}
