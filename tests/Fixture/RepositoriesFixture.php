<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RepositoriesFixture
 *
 */
class RepositoriesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'name' => ['type' => 'string', 'length' => 128, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'url' => ['type' => 'string', 'length' => 1000, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'type_vcs' => ['type' => 'string', 'length' => 45, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'is_stored' => ['type' => 'boolean', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'name_unique' => ['type' => 'unique', 'columns' => ['name'], 'length' => []],
            'url_unique' => ['type' => 'unique', 'columns' => ['url'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'name' => 'Comptoire du libre',
            'url' => 'https://gitlab.adullact.net/Comptoir/Comptoir-srv.git',
            'type_vcs' => 'git',
            'is_stored' => 1,
        ],
        [
            "name" => "oh-my-zsh",
            "url" => "https://github.com/robbyrussell/oh-my-zsh.git",
            'type_vcs' => 'git',
            'is_stored' => 0
        ],
        [
            "name" => "i-delibre",
            "url" => "https://scm.adullact.net/anonscm/svn/idelibre",
            'type_vcs' => 'svn',
            'is_stored' => 0
        ],
        [
            "name" => "QOSS",
            "url" => "git@gitlab.adullact.net:Quantifed-OSS/Quantified-OSS.git",
            'type_vcs' => 'git',
            'is_stored' => 0
        ],


    ];
}
