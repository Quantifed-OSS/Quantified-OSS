<?php
namespace App\Test\Fixture;


use Cake\I18n\Time;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * RawInitialValuesFixture
 *
 */
class RawInitialValuesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'repository_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'riv_nb_commit_1m' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'riv_nb_commit_12m' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'riv_nb_contributors' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'riv_total_commit' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'riv_nb_commit_highest_committer' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'riv_last_commit_date' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'riv_first_commit_date' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'repository_riv_fk' => ['type' => 'foreign', 'columns' => ['repository_id'], 'references' => ['repositories', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public function init()
    {
        $this->records = [
            [
                'created' => new Time('60 days ago'),
                'riv_nb_commit_1m' => 15,
                'riv_nb_commit_12m' => 120,
                'riv_nb_contributors' => 5,
                'riv_total_commit' => 1,
                'riv_nb_commit_highest_committer' => 1,
                'riv_last_commit_date' => new Time('now'),
                'riv_first_commit_date' => new Time('2 years ago'),
                'repository_id' => 1
            ],
            [
                'created' => new Time('12 days ago'),
                'riv_nb_commit_1m' => 10,
                'riv_nb_commit_12m' => 439,
                'riv_nb_contributors' => 6,
                'riv_total_commit' => 475,
                'riv_nb_commit_highest_committer' => 330,
                'riv_last_commit_date' => new Time('2 month ago'),
                'riv_first_commit_date' => new Time('5 years ago'),
                'repository_id' => 1
            ],
            [
                'created' => new Time('15 days ago'),
                'riv_nb_commit_1m' => 10,
                'riv_nb_commit_12m' => 439,
                'riv_nb_contributors' => 7,
                'riv_total_commit' => 475,
                'riv_nb_commit_highest_committer' => 330,
                'riv_last_commit_date' => new Time('2 month ago'),
                'riv_first_commit_date' => new Time('5 years ago'),
                'repository_id' => 1
            ],
            [
                'created' => new Time('10 days ago'),
                'riv_nb_commit_1m' => 10,
                'riv_nb_commit_12m' => 439,
                'riv_nb_contributors' => 8,
                'riv_total_commit' => 475,
                'riv_nb_commit_highest_committer' => 330,
                'riv_last_commit_date' => new Time('2 month ago'),
                'riv_first_commit_date' => new Time('5 years ago'),
                'repository_id' => 1
            ],
            [
                'created' => new Time('10 days ago'),
                'riv_nb_commit_1m' => 60,
                'riv_nb_commit_12m' => 650,
                'riv_nb_contributors' => 120,
                'riv_total_commit' => 600,
                'riv_nb_commit_highest_committer' => 330,
                'riv_last_commit_date' => new Time('3 month ago'),
                'riv_first_commit_date' => new Time('3 years ago'),
                'repository_id' => 2
            ]
        ];
        parent::init();
    }
}
