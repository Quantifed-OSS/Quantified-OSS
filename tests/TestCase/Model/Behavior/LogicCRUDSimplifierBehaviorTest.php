<?php

namespace App\Test\TestCase\Model\Behavior;

use App\Model\Behavior\LogicCRUDSimplifierBehavior;
use App\Test\Fixture\RepositoriesFixture;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Behavior\LogicCRUDSimplifierBehavior Test Case
 * @property \Cake\ORM\Table Repositories
 */
class LogicCRUDSimplifierBehaviorTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Behavior\LogicCRUDSimplifierBehavior
     */
    public $LogicCRUDSimplifierBehavior;


    public $fixtures = [
        'app.repositories',
        'app.marks',
        'app.raw_final_values',
        'app.averages',
        'app.raw_initial_values'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $config = TableRegistry::exists('Repositories') ? [] : ['className' => 'App\Model\Table\RepositoriesTable'];
        $this->Repositories = TableRegistry::get('Repositories', $config);

        $this->LogicCRUDSimplifierBehavior = new LogicCRUDSimplifierBehavior($this->Repositories);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LogicCRUDSimplifierBehavior);
        unset($this->Repositories);

        parent::tearDown();
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAddWithNameNotUnique()
    {
        $expected = false;

        $actual = $this->LogicCRUDSimplifierBehavior->add([
                'name' => 'Comptoire du libre',
                'url' => 'Lorem ipsum dolor sit ',
                'type_vcs' => 'Lorem ipsum dolor sit amet',
                'is_stored' => true,
                'created' => null,
            ]
        );

        $this->assertEquals($expected, $actual->isSuccess());
    }


    public function testAddWithUrlNotUnique()
    {
        $expected = false;

        $actual = $this->LogicCRUDSimplifierBehavior->add([
                'name' => 'Lorem ipsum dolor sit ',
                'url' => 'https://gitlab.adullact.net/Comptoir/Comptoir-srv.git',
                'type_vcs' => 'Lorem ipsum dolor sit amet',
                'is_stored' => true,
                'created' => null,
            ]
        );

        $this->assertEquals($expected, $actual->isSuccess());
    }

    public function testMultipleAddWithUrlNotUniqueAndUnique()
    {
        $number_repositories_before = count($this->Repositories->find()->toArray());

        $this->LogicCRUDSimplifierBehavior->add([
                'name' => 'Lorem ipsum dolor sit ',
                'url' => 'https://gitlab.adullact.net/Comptoir/Comptoir-srv.git',
                'type_vcs' => 'Lorem ipsum dolor sit amet',
                'is_stored' => true,
                'created' => null,
            ]
        );

        $this->LogicCRUDSimplifierBehavior->add([
                'name' => 'Lorem ipsum dolor sit ',
                'url' => 'git@gitlab.adullact.net:E-Collectivite/E-Collectivite-server.git',
                'type_vcs' => 'Lorem ipsum dolor sit amet',
                'is_stored' => true,
                'created' => null,
            ]
        );

        $number_repositories_after = count($this->Repositories->find()->toArray());

        $this->assertEquals($number_repositories_before + 1, $number_repositories_after);
    }

    public function testMultipleAddWithNameNotUniqueAndUnique()
    {
        $number_repositories_before = count($this->Repositories->find()->toArray());

        $this->LogicCRUDSimplifierBehavior->add([
                'name' => 'QOSS',
                'url' => 'https://gitlab.adullact.net/Comptoir/Comptoir-srv.git',
                'type_vcs' => 'Lorem ipsum dolor sit amet',
                'is_stored' => true,
                'created' => null,
            ]
        );

        $this->LogicCRUDSimplifierBehavior->add([
                'name' => 'Lorem ipsum dolor sit ',
                'url' => 'git@gitlab.adullact.net:E-Collectivite/E-Collectivite-server.git',
                'type_vcs' => 'Lorem ipsum dolor sit amet',
                'is_stored' => true,
                'created' => null,
            ]
        );

        $number_repositories_after = count($this->Repositories->find()->toArray());

        $this->assertEquals($number_repositories_before + 1, $number_repositories_after);
    }

    public function testAddTrue()
    {
        $number_of_current_repos = count($this->Repositories->find()->toArray());

        $expected = [
            'id' => $number_of_current_repos + 1,
            'name' => 'Lorem ipsum dolor sit',
            'url' => 'Lorem ipsum dolor sit',
            'type_vcs' => 'Lorem ipsum dolor sit amet',
            'is_stored' => true,
            'created' => null,
        ];

        $actual = $this->LogicCRUDSimplifierBehavior->add([
                'name' => 'Lorem ipsum dolor sit',
                'url' => 'Lorem ipsum dolor sit',
                'type_vcs' => 'Lorem ipsum dolor sit amet',
                'is_stored' => true,
                'created' => null,
            ]
        );

        $this->assertEquals($expected, $actual->getObject()->toArray());
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
