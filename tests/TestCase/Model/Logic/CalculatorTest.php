<?php
/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 09/05/17
 * Time: 09:39
 */

namespace App\Test\TestCase\Model\Logic;

use App\Model\Logic\Compute\Calculator;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;


/**
 * @property \Cake\ORM\Table RawFinalValues
 * @property \Cake\ORM\Table RawInitialValues
 * @property \Cake\ORM\Table Repositories
 */
class CalculatorTest extends TestCase
{
    public $Calculator;

    public $fixtures = [
        'app.repositories',
        'app.marks',
        'app.raw_final_values',
        'app.averages',
        'app.raw_initial_values'
    ];

    public function setUp()
    {
        $config = TableRegistry::exists('RawFinalValues') ? [] : ['className' => 'App\Model\Table\RawFinalValuesTable'];
        $this->RawFinalValues = TableRegistry::get('RawFinalValues', $config);

        $config = TableRegistry::exists('RawInitialValues') ? [] : ['className' => 'App\Model\Table\RawInitialValuesTable'];
        $this->RawInitialValues = TableRegistry::get('RawInitialValues', $config);

        $config = TableRegistry::exists('Repositories') ? [] : ['className' => 'App\Model\Table\RepositoriesTable'];
        $this->Repositories = TableRegistry::get('Repositories', $config);

        parent::setUp();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RawFinalValues);
        unset($this->RawInitialValues);
        unset($this->Repositories);
        unset($this->Compute);
        parent::tearDown();
    }

    public function testRawFinalValuesOfRepository_id1WithAverage_id1()
    {

        $expected = ['rfv_last_commit_age' => (int)61,
            'rfv_project_age' => (int)1826,
            'rfv_nb_contributors' => (int)8,
            'rfv_delta_commit_1m' => (float)0.2,
            'rfv_delta_commit_12m' => (float)1.254,
            'rfv_highest_committer_percent' => (float)0.695,
            'average_id' => (int)1,
            'repository_id' => (int)1,

            'raw_initial_value_id' => (int)4
        ];

        $actual = Calculator::computeRFVAndMarks(1);

        self::assertEquals($expected, $actual->getObject()['raw_final_values']);
    }

    public function testMarksOfRepository_id1WithAverage_id1()
    {

        $expected = ['m_last_commit_age' => (int)2,
            'm_project_age' => (int)3,
            'm_nb_contributors' => (int)1,
            'm_delta_commit_1m' => (int)0,
            'm_delta_commit_12m' => (int)2,
            'm_highest_committer_percent' => (int)1,
            'repository_id' => (int)1,
        ];

        $actual = Calculator::computeRFVAndMarks(1);

        self::assertEquals($expected, $actual->getObject()['marks']);
    }


}
