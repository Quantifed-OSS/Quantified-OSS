<?php
/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 25/04/17
 * Time: 14:29
 */

namespace App\Test\TestCase\Model\Logic;

use App\Model\Logic\PreCompute\Logs\LogsReader;
use Cake\I18n\FrozenTime;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Webcreate\Vcs\Git;

/**
 * @property \Cake\ORM\Table Repositories
 */
class LogsReaderTest extends TestCase
{

    public $LogsReader;

    public $timeFirstCommit;

    public $timeLastCommit;

    public $fixtures = [
        'app.repositories',
        'app.marks',
        'app.raw_final_values',
        'app.averages',
        'app.raw_initial_values'
    ];

    public $logs;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->timeFirstCommit = new FrozenTime('2 years ago');

        $this->timeLastCommit = new FrozenTime ('2 hours ago');

        /*
         * nb_total_commit = 11
         * nb_contributors = 2
         * nb_commit_highest_committer = 8
         * nb_commit_1m = 7
         * nb_commit_12m = 9
         * last_commit_date = new Time ('2 hours ago')
         * first_commit_date = new Time ('380 days ago')
         *
         * */

        $this->logs = [
            0 => [
                'date' => $this->timeLastCommit,
                'author' => 'Matthieu FAURE'
            ],
            1 => [
                'date' => new FrozenTime ('2 days ago'),
                'author' => 'Matthieu FAURE'
            ],
            2 => [
                'date' => new FrozenTime ('50 days ago'),
                'author' => 'Matthieu FAURE'
            ],
            3 => [
                'date' => new FrozenTime ('42 days ago'),
                'author' => 'Pastor Mickaël'
            ],
            4 => [
                'date' => new FrozenTime ('150 days ago'),
                'author' => 'Pastor Mickaël'
            ],
            5 => [
                'date' => new FrozenTime ('18 days ago'),
                'author' => 'Pastor Mickaël'
            ],
            6 => [
                'date' => new FrozenTime ('164 days ago'),
                'author' => 'Pastor Mickaël'
            ],
            7 => [
                'date' => new FrozenTime ('23 days ago'),
                'author' => 'Pastor Mickaël'
            ],
            8 => [
                'date' => new FrozenTime ('5 days ago'),
                'author' => 'Pastor Mickaël'
            ],
            9 => [
                'date' => $this->timeFirstCommit,
                'author' => 'Pastor Mickaël'
            ],
            10 => [
                'date' => new FrozenTime ('7 days ago'),
                'author' => 'Pastor Mickaël'
            ],
        ];

    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LogsReader);
        unset($this->Repositories);

        parent::tearDown();
    }

    public function testLogs()
    {
        $actual = LogsReader::getRawInitialValues($this->logs);

        $expected = [
            'riv_nb_commit_1m' => (int)6,
            'riv_nb_commit_12m' => (int)10,
            'riv_nb_contributors' => (int)2,
            'riv_total_commit' => (int)11,
            'riv_nb_commit_highest_committer' => (int)8,
            'riv_last_commit_date' => $this->timeLastCommit,
            'riv_first_commit_date' => $this->timeFirstCommit
        ];

        self::assertEquals($expected, $actual);
    }

}
