<?php
/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 27/04/17
 * Time: 14:28
 */

namespace App\Test\TestCase\Model\Logic;

use App\Model\Logic\Divisor\Divisor;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;


/**
 * @property \Cake\ORM\Table RawInitialValues
 */
class DivisorTest extends TestCase
{
    public $Divisor;

    public $fixtures = [
        'app.repositories',
        'app.marks',
        'app.raw_final_values',
        'app.averages',
        'app.raw_initial_values'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RawInitialValues') ? [] : ['className' => 'App\Model\Table\RawInitialValuesTable'];
        $this->RawInitialValues = TableRegistry::get('RawInitialValues', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Divisor);
        unset($this->RawInitialValues);
        parent::tearDown();
    }

    public function testNumberOfRIVWhenSelectInitialValuesWithinLast30day()
    {
        $divisor = new Divisor();
        $data = $this->invokeMethod($divisor, 'selectRawInitialValuesWithinLast', [30]);

        $number_of_initial_value_selected = 0;
        foreach ($data as $repos_data) {
            $number_of_initial_value_selected += count($repos_data['raw_initial_values']);
        }

        self::assertEquals(4, $number_of_initial_value_selected);
    }

    public function testAverage()
    {
        $average = TableRegistry::get('Averages');

        $averages_before_disivor = count($average->find('all')->toArray());

        $divisor = new Divisor();
        $divisor->launchDivisor();
        $averages_after_divisor = count($average->find('all')->toArray());

        self::assertEquals($averages_before_disivor + 1, $averages_after_divisor);
    }

    // Used to test private/protected method
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
