<?php
/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 24/04/17
 * Time: 14:31
 */

namespace App\Test\TestCase\Model\Logic;

use App\Model\Logic\PreCompute\Updater\UpdateNotStockedGit;
use App\Model\Logic\PreCompute\Updater\Updater;
use App\Model\Logic\PreCompute\Updater\UpdateStockedGit;
use App\Model\Logic\PreCompute\Updater\UpdateSvn;
use App\Model\Logic\PreCompute\VCSInterface\GIT_webcreate;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\ORM\Exception\MissingTableClassException;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Cake\Utility\Inflector;
use Webcreate\Vcs\Git;
use Webcreate\Vcs\Svn;

/**
 * @property \Cake\ORM\Table Repositories
 */
class UpdaterTest extends TestCase
{
    public $Updater;

    public $Repositories;

    public $fixtures = [
        'app.repositories',
        'app.marks',
        'app.raw_final_values',
        'app.averages',
        'app.raw_initial_values'
    ];

    public $logs;
    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $config = TableRegistry::exists('Repositories') ? [] : ['className' => 'App\Model\Table\RepositoriesTable'];
        $this->Repositories = TableRegistry::get('Repositories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Updater);
        unset($this->Repositories);

        parent::tearDown();
    }

    // Test private/protected method
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    /*   public function testRrmdir()
       {

           $strategy = new UpdateStockedGit();
           $this->Updater = new Updater($strategy, $this->Repositories->get(1));
           $this->Updater->updateRepository();

           $dir = Configure::read('Clone_path');
           $path = $dir . '/Comptoire du libre';

           self::assertTrue(is_dir($path));
           $dir_number_before_delete = count(scandir($dir));

           $s = new UpdateNotStockedGit();
           //$s->rrmdir($path);
           $this->invokeMethod($s, 'rrmdir', [$path]);

           $dir_number_after_delete = count(scandir($dir));

           self::assertFalse(is_dir($path));
           self::assertEquals($dir_number_before_delete - 1, $dir_number_after_delete);

       }*/


    public function testUpdateNotStockedGitMock()
    {
        $strategy = new UpdateNotStockedGit();

        $repos = $this->Repositories->add(
            [
                'name' => 'test2',
                'url' => "someUrl",
                'type_vcs' => "mock",
                'is_stored' => false
            ]
        );

        $this->Updater = new Updater($strategy, $repos->getObject());
        $actual = $this->Updater->updateRepository();

        $expected = ['somelogs'];

        self::assertEquals($expected, $actual->getObject());
    }

    public function testUpdateStockedGitMock()
    {
        $strategy = new UpdateStockedGit();

        $repos = $this->Repositories->add(
            [
                'name' => 'test2',
                'url' => "someUrl",
                'type_vcs' => "mock",
                'is_stored' => true
            ]
        );

        $this->Updater = new Updater($strategy, $repos->getObject());
        $actual = $this->Updater->updateRepository();

        $expected = ['somelogs'];

        self::assertEquals($expected, $actual->getObject());
    }

    public function testUpdateSvnMock()
    {
        $strategy = new UpdateStockedGit();

        $repos = $this->Repositories->add(
            [
                'name' => 'test2',
                'url' => "someUrl",
                'type_vcs' => "mock",
                'is_stored' => false
            ]
        );

        $this->Updater = new Updater($strategy, $repos->getObject());
        $actual = $this->Updater->updateRepository();

        $expected = ['somelogs'];

        self::assertEquals($expected, $actual->getObject());
    }


    // Long test duration ////////////////////////////////////////////////////////////

    /*    public function testUpdateNotStockedGit()
           {
               $strategy = new UpdateNotStockedGit();
               $this->Updater = new Updater($strategy, $this->Repositories->get(4));
               $actual = $this->Updater->updateRepository();

               $git = new Git('git@gitlab.adullact.net:Quantifed-OSS/Quantified-OSS.git');
               $logs = $git->log('.');
               $expected = array();
               foreach ($logs as $commit) {
                   array_push($expected,
                       [
                           'date' => $commit->getDate(),
                           'author' => $commit->getAuthor(),
                       ]
                   );
               }

               self::assertEquals($expected, $actual);
           }

           public function testUpdateStockedGit()
           {
               $strategy = new UpdateStockedGit();
               $this->Updater = new Updater($strategy, $this->Repositories->get(1));
               $actual = $this->Updater->updateRepository();

               $git = new Git('https://gitlab.adullact.net/Comptoir/Comptoir-srv.git');
               $logs = $git->log('.');
               $expected = array();
               foreach ($logs as $commit) {
                   array_push($expected,
                       [
                           'date' => $commit->getDate(),
                           'author' => $commit->getAuthor(),
                       ]
                   );
               }

               self::assertEquals($expected, $actual);
           }

           public function testUpdateSvn()
           {
               $strategy = new UpdateSvn();
               $this->Updater = new Updater($strategy, $this->Repositories->get(3));
               $actual = $this->Updater->updateRepository();

               $svn = new Svn('https://scm.adullact.net/anonscm/svn/idelibre');
               $logs = $svn->log('.');
               $expected = array();
               foreach ($logs as $commit) {
                   array_push($expected,
                       [
                           'date' => $commit->getDate(),
                           'author' => $commit->getAuthor(),
                       ]
                   );
               }

               self::assertEquals($expected, $actual);
           }*/

}
