<?php
/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 25/04/17
 * Time: 16:05
 */

namespace App\Test\TestCase\Model\Logic;

use App\Model\Logic\PreCompute\PreCompute;
use App\Model\Logic\ResponseData;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Webcreate\Vcs\Git;


/**
 * Class PreComputeTest
 * @property \Cake\ORM\Table RawInitialValues
 * @property \Cake\ORM\Table Repositories
 * @package App\Test\TestCase\Model\Logic
 */
class PreComputeTest extends TestCase
{
    public $PreCompute;

    public $fixtures = [
        'app.repositories',
        'app.marks',
        'app.raw_final_values',
        'app.averages',
        'app.raw_initial_values'
    ];

    public $logs;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RawInitialValues') ? [] : ['className' => 'App\Model\Table\RawInitialValuesTable'];
        $this->RawInitialValues = TableRegistry::get('RawInitialValues', $config);

        $config = TableRegistry::exists('Repositories') ? [] : ['className' => 'App\Model\Table\RepositoriesTable'];
        $this->Repositories = TableRegistry::get('Repositories', $config);

        $this->PreCompute = new PreCompute();

        $timeFirstCommit = new Time ('2 years ago');

        $timeLastCommit = new Time ('2 hours ago');

        /*
         * nb_total_commit = 11
         * nb_contributors = 2
         * nb_commit_highest_committer = 8
         * nb_commit_1m = 7
         * nb_commit_12m = 9
         * last_commit_date = new Time ('2 hours ago')
         * first_commit_date = new Time ('380 days ago')
         *
         * */

        $this->logs = [
            0 => [
                'date' => $timeLastCommit,
                'author' => 'Matthieu FAURE'
            ],
            1 => [
                'date' => new Time ('2 days ago'),
                'author' => 'Matthieu FAURE'
            ],
            2 => [
                'date' => new Time ('50 days ago'),
                'author' => 'Matthieu FAURE'
            ],
            3 => [
                'date' => new Time ('42 days ago'),
                'author' => 'Pastor Mickaël'
            ],
            4 => [
                'date' => new Time ('150 days ago'),
                'author' => 'Pastor Mickaël'
            ],
            5 => [
                'date' => new Time ('18 days ago'),
                'author' => 'Pastor Mickaël'
            ],
            6 => [
                'date' => new Time ('164 days ago'),
                'author' => 'Pastor Mickaël'
            ],
            7 => [
                'date' => new Time ('23 days ago'),
                'author' => 'Pastor Mickaël'
            ],
            8 => [
                'date' => new Time ('5 days ago'),
                'author' => 'Pastor Mickaël'
            ],
            9 => [
                'date' => $timeFirstCommit,
                'author' => 'Pastor Mickaël'
            ],
            10 => [
                'date' => new Time ('7 days ago'),
                'author' => 'Pastor Mickaël'
            ],
        ];
    }

    public function tearDown()
    {
        unset($this->RawInitialValues);
        unset($this->Repositories);

        parent::tearDown();
    }

    public function testLaunchPreComputeMock()
    {
        $riv_number_before = count($this->RawInitialValues->find()->toArray());

        $mock = $this->getMockBuilder(PreCompute::class)
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->setMethods(['getLogsFromRepository', 'insertRIVintoDB'])
            ->getMock();
        $mock->expects($this->once())
            ->method('getLogsFromRepository')
            ->willReturn(new ResponseData(true, 'Logs mock success', $this->logs));

        $mock->launchPreCompute($this->Repositories->get(1));

        $riv_number_after = count($this->RawInitialValues->find()->toArray());

        self::assertEquals($riv_number_before + 1, $riv_number_after);
    }

    public function testLaunchPreComputeOnAllRepositoriesMock()
    {
        $mock = $this->getMockBuilder(PreCompute::class)
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->setMethods(['launchPreCompute'])
            ->getMock();
        $mock->expects($this->exactly(4))
            ->method('launchPreCompute');

        $mock->launchPreComputeOnAllRepositories();
    }

    public function testWrongUrlWithNotStockedGit()
    {
        $number_riv_before_compute = count($this->RawInitialValues->find()->toArray());

        $this->Repositories->add(
            [
                'name' => 'test2',
                'url' => "wrongUrl",
                'type_vcs' => "git",
                'is_stored' => false
            ]
        );

        $repos = $this->Repositories->find()->where(['name =' => 'test2'])->first();

        $this->PreCompute->launchPreCompute($repos);

        $number_riv_after_compute = count($this->RawInitialValues->find()->toArray());

        self::assertEquals($number_riv_before_compute, $number_riv_after_compute);
    }

    public function testWrongUrlWithStockedGitNotYetCloned()
    {
        $number_riv_before_compute = count($this->RawInitialValues->find()->toArray());

        $this->Repositories->add(
            [
                'name' => 'test2',
                'url' => "wrongUrl",
                'type_vcs' => "git",
                'is_stored' => true
            ]
        );

        $repos = $this->Repositories->find()->where(['name =' => 'test2'])->first();

        $this->PreCompute->launchPreCompute($repos);

        $number_riv_after_compute = count($this->RawInitialValues->find()->toArray());

        self::assertEquals($number_riv_before_compute, $number_riv_after_compute);
    }

    /*    public function testProblematicCharacterInComment()
        {
            $number_riv_before_compute = count($this->RawInitialValues->find()->toArray());

            $this->Repositories->add(
                [
                    'name' => 'gecko',
                    'url' => "https://github.com/mozilla/gecko-dev.git",
                    'type_vcs' => "git",
                    'is_stored' => true
                ]
            );

            $repos = $this->Repositories->find()->where(['name =' => 'gecko'])->first();

            $res = $this->PreCompute->launchPreCompute($repos);

            debug($res);

            $number_riv_after_compute = count($this->RawInitialValues->find()->toArray());

            self::assertEquals($number_riv_before_compute + 1, $number_riv_after_compute);
        }*/

    /*    public function testProblematicCharacterInComment()
                {
                    $number_riv_before_compute = count($this->RawInitialValues->find()->toArray());

                    $this->Repositories->add(
                        [
                            'name' => 'lut',
                            'url' => "https://github.com/lutece-platform/lutece-core.git",
                            'type_vcs' => "git",
                            'is_stored' => false
                        ]
                    );

                    $repos = $this->Repositories->find()->where(['name =' => 'lut'])->first();

                    $this->PreCompute->launchPreCompute($repos);

                    $number_riv_after_compute = count($this->RawInitialValues->find()->toArray());

                    self::assertEquals($number_riv_before_compute+1, $number_riv_after_compute);
                }*/

    /*        public function testBigReposWithLibreOffice()
            {
                $number_riv_before_compute = count($this->RawInitialValues->find()->toArray());

                $this->Repositories->add(
                    [
                        'name' => 'core',
                        'url' => "https://github.com/LibreOffice/core.git",
                        'type_vcs' => "git",
                        'is_stored' => true
                    ]
                );

                $repos = $this->Repositories->find()->where(['name =' => 'core'])->first();

                $this->PreCompute->launchPreCompute($repos);

                $number_riv_after_compute = count($this->RawInitialValues->find()->toArray());

                //debug($this->RawInitialValues->get(6)->toArray());

                self::assertEquals($number_riv_before_compute+1, $number_riv_after_compute);
            }*/

}
