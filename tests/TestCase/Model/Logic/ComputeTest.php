<?php
/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 02/05/17
 * Time: 16:10
 */

namespace App\Test\TestCase\Model\Logic;

use App\Model\Logic\Compute\Compute;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use DateTime;

/**
 * @property \Cake\ORM\Table RawInitialValues
 * @property \Cake\ORM\Table Repositories
 * @property \Cake\ORM\Table RawFinalValues
 * @property \Cake\ORM\Table Marks
 */
class ComputeTest extends TestCase
{
    public $Compute;

    public $fixtures = [
        'app.repositories',
        'app.marks',
        'app.raw_final_values',
        'app.averages',
        'app.raw_initial_values'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        $config = TableRegistry::exists('RawFinalValues') ? [] : ['className' => 'App\Model\Table\RawFinalValuesTable'];
        $this->RawFinalValues = TableRegistry::get('RawFinalValues', $config);
        parent::setUp();

        $config = TableRegistry::exists('Marks') ? [] : ['className' => 'App\Model\Table\MarksTable'];
        $this->Marks = TableRegistry::get('Marks', $config);
        parent::setUp();

        $config = TableRegistry::exists('RawInitialValues') ? [] : ['className' => 'App\Model\Table\RawInitialValuesTable'];
        $this->RawInitialValues = TableRegistry::get('RawInitialValues', $config);
        parent::setUp();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RawFinalValues);
        unset($this->RawInitialValues);
        unset($this->Compute);
        parent::tearDown();
    }

    public function testNumberOfNewRawFinalValuesDataBaseEntryWhenUsingCompute_launchComputeOnAllRepositories()
    {
        $number_repositories_with_riv = count($this->RawInitialValues
            ->find('all')
            ->distinct('repository_id')
            ->where(['RawInitialValues.created >' => new DateTime('-' . 30 . ' days')])
            ->toArray());
        $number_rfv_before_compute = count($this->RawFinalValues->find()->toArray());

        $compute = new Compute();
        $compute->launchComputeOnAllRepositories();

        $number_rfv_after_compute = count($this->RawFinalValues->find('all')->toArray());
        $number_rfv_expected = $number_repositories_with_riv + $number_rfv_before_compute;

        self::assertEquals($number_rfv_expected, $number_rfv_after_compute);
    }

    public function testNumberOfNewMarksDataBaseEntryWhenUsingCompute_launchComputeOnAllRepositories()
    {
        $number_repositories_with_riv = count($this->RawInitialValues
            ->find('all')
            ->distinct('repository_id')
            ->where(['RawInitialValues.created >' => new DateTime('-' . 30 . ' days')])
            ->toArray());
        $number_rfv_before_compute = count($this->Marks->find()->toArray());

        $compute = new Compute();
        $compute->launchComputeOnAllRepositories();

        $number_rfv_after_compute = count($this->Marks->find('all')->toArray());
        $number_rfv_expected = $number_repositories_with_riv + $number_rfv_before_compute;

        self::assertEquals($number_rfv_expected, $number_rfv_after_compute);
    }
}
