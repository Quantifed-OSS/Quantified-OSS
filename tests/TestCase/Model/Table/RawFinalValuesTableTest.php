<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RawFinalValuesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RawFinalValuesTable Test Case
 */
class RawFinalValuesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RawFinalValuesTable
     */
    public $RawFinalValues;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.raw_final_values',
        'app.averages',
        'app.raw_initial_values',
        'app.repositories',
        'app.marks'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RawFinalValues') ? [] : ['className' => 'App\Model\Table\RawFinalValuesTable'];
        $this->RawFinalValues = TableRegistry::get('RawFinalValues', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RawFinalValues);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
