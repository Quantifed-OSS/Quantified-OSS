<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RawInitialValuesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RawInitialValuesTable Test Case
 */
class RawInitialValuesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RawInitialValuesTable
     */
    public $RawInitialValues;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.raw_initial_values',
        'app.repositories',
        'app.marks',
        'app.raw_final_values',
        'app.averages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RawInitialValues') ? [] : ['className' => 'App\Model\Table\RawInitialValuesTable'];
        $this->RawInitialValues = TableRegistry::get('RawInitialValues', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RawInitialValues);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

}
