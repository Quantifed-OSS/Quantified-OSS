<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AveragesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AveragesTable Test Case
 */
class AveragesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AveragesTable
     */
    public $Averages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.averages',
        'app.raw_final_values',
        'app.raw_initial_values',
        'app.repositories',
        'app.marks'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Averages') ? [] : ['className' => 'App\Model\Table\AveragesTable'];
        $this->Averages = TableRegistry::get('Averages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Averages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

}
