<?php
/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 11/05/17
 * Time: 15:34
 */

use App\Controller\Api\V1\APIController;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * @property \Cake\ORM\Table RawFinalValues
 * @property \Cake\ORM\Table RawInitialValues
 * @property \Cake\ORM\Table Repositories
 */
class APIControllerTest extends TestCase
{
    public $APIController;

    public $fixtures = [
        'app.repositories',
        'app.marks',
        'app.raw_final_values',
        'app.averages',
        'app.raw_initial_values'
    ];

    public function setUp()
    {
        $config = TableRegistry::exists('RawFinalValues') ? [] : ['className' => 'App\Model\Table\RawFinalValuesTable'];
        $this->RawFinalValues = TableRegistry::get('RawFinalValues', $config);

        $config = TableRegistry::exists('RawInitialValues') ? [] : ['className' => 'App\Model\Table\RawInitialValuesTable'];
        $this->RawInitialValues = TableRegistry::get('RawInitialValues', $config);

        $config = TableRegistry::exists('Repositories') ? [] : ['className' => 'App\Model\Table\RepositoriesTable'];
        $this->Repositories = TableRegistry::get('Repositories', $config);

        $this->APIController = new APIController();

        parent::setUp();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RawFinalValues);
        unset($this->RawInitialValues);
        unset($this->Repositories);
        unset($this->APIController);
        parent::tearDown();
    }


    public function testSaveRepositoryAndPreComputeAndComputeEntries()
    {
        $data = [
            [
                'name' => 'testName',
                'url' => 'https://yliebart@gitlab.adullact.net/Quantifed-OSS/Quantified-OSS.git',
                'is_stored' => false,
                'type_vcs' => 'git'
            ],
            [
                'name' => 'testName2',
                'url' => 'https://scm.adullact.net/anonscm/svn/webcil',
                'is_stored' => false,
                'type_vcs' => 'svn'
            ]
        ];

        $repos_number_before_save = count($this->Repositories->find()->toArray());
        $riv_number_before_save = count($this->RawInitialValues->find()->toArray());
        $rfv_number_before_save = count($this->RawFinalValues->find()->toArray());

        $this->invokeMethod($this->APIController, 'saveAndCompute', [$data]);

        $repos_number_after_save = count($this->Repositories->find()->toArray());
        $riv_number_after_save = count($this->RawInitialValues->find()->toArray());
        $rfv_number_after_save = count($this->RawFinalValues->find()->toArray());

        self::assertEquals($repos_number_before_save + 2, $repos_number_after_save);
        self::assertEquals($riv_number_before_save + 2, $riv_number_after_save);
        self::assertEquals($rfv_number_before_save + 2, $rfv_number_after_save);
    }

    public function testSaveRepositoryAndPreComputeAndComputeEntriesWithOneFalse()
    {
        $data = [
            [//should not pass (already in fixture)
                'name' => 'testName2',
                'url' => 'https://gitlab.adullact.net/Comptoir/Comptoir-srv.git',
                'is_stored' => false,
                'type_vcs' => 'git'
            ],
            [//should pass
                'name' => 'testName',
                'url' => 'https://yliebart@gitlab.adullact.net/Quantifed-OSS/Quantified-OSS.git',
                'is_stored' => false,
                'type_vcs' => 'git'
            ],
        ];

        $repos_number_before_save = count($this->Repositories->find()->toArray());
        $riv_number_before_save = count($this->RawInitialValues->find()->toArray());
        $rfv_number_before_save = count($this->RawFinalValues->find()->toArray());

        $this->invokeMethod($this->APIController, 'saveAndCompute', [$data]);

        $repos_number_after_save = count($this->Repositories->find()->toArray());
        $riv_number_after_save = count($this->RawInitialValues->find()->toArray());
        $rfv_number_after_save = count($this->RawFinalValues->find()->toArray());

        self::assertEquals($repos_number_before_save + 1, $repos_number_after_save);
        self::assertEquals($riv_number_before_save + 1, $riv_number_after_save);
        self::assertEquals($rfv_number_before_save + 1, $rfv_number_after_save);
    }

    public function testGetLastValuesFromRepositories()
    {
        $repos = [0 => ['name' => 'Comptoire du libre']];

        //$actual = $this->APIController->getLastValuesFromRepositories($repos);

        $actual = $this->invokeMethod($this->APIController, "getLastValuesFromRepositories", [$repos]);

        self::assertEquals(4, $actual[0]['raw_initial_value_id']);
        self::assertEquals(1, $actual[0]['average_id']);
        self::assertEquals(1, $actual[0]['repository_id']);
        self::assertEquals(1, $actual[0]['marks'][0]['raw_final_value_id']);
        self::assertTrue($actual[0]['created']->wasWithinLast('3 days'));
        self::assertTrue($actual[0]['marks'][0]['created']->wasWithinLast('3 days'));
        self::assertTrue($actual[0]['average']['created']->wasWithinLast('1 hour'));
        self::assertTrue($actual[0]['raw_initial_value']['riv_last_commit_date']->wasWithinLast('62 days'));
        self::assertTrue($actual[0]['raw_initial_value']['riv_first_commit_date']->wasWithinLast('6 years'));
    }

    // Test private/protected method
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
