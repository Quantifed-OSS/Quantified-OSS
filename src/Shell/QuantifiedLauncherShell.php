<?php
/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 29/05/17
 * Time: 10:25
 */

namespace App\Shell;

use App\Model\Logic\Compute\Compute;
use App\Model\Logic\Divisor\Divisor;
use App\Model\Logic\PreCompute\PreCompute;
use Cake\Console\Shell;


/**
 * @property \App\Model\Logic\PreCompute\PreCompute $PreCompute
 * @property \App\Model\Logic\Compute\Compute $Compute
 * @property \App\Model\Logic\Divisor\Divisor $Divisor
 */
class QuantifiedLauncherShell extends Shell
{
    private $PreCompute;
    private $Divisor;
    private $Compute;

    public function initialize()
    {
        parent::initialize();

        $this->PreCompute = new PreCompute();
        $this->Divisor = new Divisor();
        $this->Compute = new Compute();
    }

    /**
     * Use this function to execute PreCompute on all repositories.
     */
    public function startPreCompute()
    {
        $result = $this->PreCompute->launchPreComputeOnAllRepositories();

        $completed = $this->countCompletion($result);

        $this->out('PreCompute operation completed ' . $completed . '/' . count($result));
    }

    /**
     * Use this function to execute Divisor.
     */
    public function startDivisor()
    {
        if ($this->Divisor->launchDivisor()) {
            $this->out('Divisor operation complete');
        } else {
            $this->out('Divisor operation Failed');
        }
    }

    /**
     * Use this function to execute Compute on all repositories.
     */
    public function startCompute()
    {
        $result = $this->Compute->launchComputeOnAllRepositories();

        $completed = $this->countCompletion($result);

        $this->out('Compute operation completed ' . $completed . '/' . count($result));
    }

    /**
     * @param $result
     * @return int
     */
    private function countCompletion($result): int
    {
        $completed = 0;

        foreach ($result as $done) {
            if ($done) {
                $completed++;
            }
        }
        return $completed;
    }

}