<?php

namespace App\Model\Logic;

use JsonSerializable;

/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 01/06/17
 * Time: 10:33
 */
class ResponseData implements JsonSerializable
{
    private $success;
    private $message;
    private $object;

    public function __construct(bool $boolean = null, String $message = null, $object = null)
    {
        $this->success = $boolean;
        $this->message = $message;
        $this->object = $object;
    }

    /**
     * @return null
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param null $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return bool
     */
    public function isNotSuccess(): bool
    {
        return !$this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;
    }

    /**
     * @return String
     */
    public function getMessage(): String
    {
        return $this->message;
    }

    /**
     * @param String $message
     */
    public function setMessage(String $message)
    {
        $this->message = $message;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'success' => $this->success,
            'message' => $this->message,
            'object' => $this->object,
        ];
    }
}