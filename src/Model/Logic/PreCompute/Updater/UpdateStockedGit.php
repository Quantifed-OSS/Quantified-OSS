<?php
/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 24/04/17
 * Time: 11:10
 */

namespace App\Model\Logic\PreCompute\Updater;

use App\Model\Logic\PreCompute\VCSInterface\VCSFactory;

class UpdateStockedGit implements I_UpdateStrategy
{

    /**
     * Create the vcs with the VCSFactory,
     * fetch the repository,
     * get the logs of the fetched repository,
     * return the logs data.
     *
     * @param Updater $updater
     * @return \App\Model\Logic\ResponseData, with the logs data of the cloned repository as object variable
     */
    public function updateRepository(Updater $updater)
    {
        $repository = $updater->getRepository();

        $vcs = VCSFactory::getInstance()->createVcs($repository->type_vcs);

        if (($repositoryResponseData = $vcs->fetchRepository($repository->url, $repository->name))->isSuccess()) {
            $logsResponseData = $vcs->getLogs($repositoryResponseData->getObject());

            return $logsResponseData;
        } else {
            return $repositoryResponseData;
        }
    }

}