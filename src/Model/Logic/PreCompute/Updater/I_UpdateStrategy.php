<?php
/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 24/04/17
 * Time: 11:08
 */

namespace App\Model\Logic\PreCompute\Updater;


use App\Model\Logic\ResponseData;

interface I_UpdateStrategy
{
    /**
     * Update the Repository of the given Updater
     *
     * @param Updater $updater
     * @return ResponseData, the logs of the updated repository.
     */
    public function updateRepository(Updater $updater);
}