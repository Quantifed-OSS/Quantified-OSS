<?php
/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 24/04/17
 * Time: 11:10
 */

namespace App\Model\Logic\PreCompute\Updater;

use App\Model\Logic\PreCompute\VCSInterface\VCSFactory;

class UpdateSvn implements I_UpdateStrategy
{
    /**
     * get the logs of the repository url,
     * return the logs data.
     *
     * @param Updater $updater
     * @return \App\Model\Logic\ResponseData, with the logs data of the cloned repository as object variable
     */
    public function updateRepository(Updater $updater)
    {
        $repository = $updater->getRepository();

        $vcs = VCSFactory::getInstance()->createVcs($repository->type_vcs);
        $logs = $vcs->getLogs($repository->url);

        return $logs;
    }
}