<?php
/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 24/04/17
 * Time: 11:06
 */

namespace App\Model\Logic\PreCompute\Updater;

use App\Model\Entity\Repository;
use App\Model\Logic\ResponseData;

/*
 * Updater use a strategy of I_UpdateStrategy to apply with a given Repository.
 * The strategy set how the Repository is supposed to be updated according to
 * it's vcs type and or if it's stored.
 *
 * */

class Updater
{
    private $strategy;
    private $repository;

    /**
     * Updater constructor.
     * @param I_UpdateStrategy $strategy , the strategy to execute to update the given repository
     * @param Repository $repository , the repository to update
     */
    public function __construct(I_UpdateStrategy $strategy, Repository $repository)
    {
        $this->strategy = $strategy;
        $this->repository = $repository;
    }

    /**
     * Update the repository according to the set strategy
     * and return the logs of the repository
     * @return ResponseData, the logs (an array described in I_VCS)
     */
    public function updateRepository()
    {
        return $this->strategy->updateRepository($this);
    }

    /**
     * @return Repository
     */
    public function getRepository()
    {
        return $this->repository;
    }

}