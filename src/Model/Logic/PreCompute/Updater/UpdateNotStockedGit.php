<?php
/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 24/04/17
 * Time: 11:10
 */

namespace App\Model\Logic\PreCompute\Updater;

use App\Model\Logic\PreCompute\VCSInterface\VCSFactory;

class UpdateNotStockedGit implements I_UpdateStrategy
{

    /**
     * Create the vcs with the VCSFactory,
     * clone the repository,
     * get the logs of the cloned repository,
     * delete recursively the cloned repository,
     * return the logs data.
     *
     * @param Updater $updater
     * @return \App\Model\Logic\ResponseData, with the logs data of the cloned repository as object variable
     */
    public function updateRepository(Updater $updater)
    {
        $repository = $updater->getRepository();

        $vcs = VCSFactory::getInstance()->createVcs($repository->type_vcs);

        if (($repositoryResponseData = $vcs->cloneRepository($repository->url, $repository->name))->isSuccess()) {
            $logs = $vcs->getLogs($repositoryResponseData->getObject());
            $this->deleteRepositoryLocalSave($repositoryResponseData->getObject());
            return $logs;
        } else {
            return $repositoryResponseData;
        }
    }

    /**
     * Delete recursively the $directory given in parameter
     * @param $repositoryPath , the path to the $directory to delete
     */
    private function deleteRepositoryLocalSave($repositoryPath)
    {
        $this->rrmdir($repositoryPath);
    }

    /**
     * Delete recursively every files and directories contained in the given $dir parameter.
     * @param String $dir , the directory to delete
     */
    private function rrmdir(String $dir)
    {
        if ($dir != null && strpos($dir, '*') === false && strlen($dir) > 0) {
            if (is_dir($dir)) {
                $objects = scandir($dir);
                foreach ($objects as $object) {
                    if ($object != "." && $object != "..") {
                        if (is_dir($dir . "/" . $object))
                            $this->rrmdir($dir . "/" . $object);
                        else
                            unlink($dir . "/" . $object);
                    }
                }
                rmdir($dir);
            }
        }
    }

}