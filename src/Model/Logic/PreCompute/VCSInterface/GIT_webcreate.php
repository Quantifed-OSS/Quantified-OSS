<?php
/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 24/04/17
 * Time: 10:00
 */

namespace App\Model\Logic\PreCompute\VCSInterface;

use App\Model\Logic\ResponseData;
use Aura\Intl\Exception;
use Cake\Core\Configure;
use Cake\Error\FatalErrorException;
use Webcreate\Util\Cli;
use Webcreate\Vcs\Common\Adapter\CliAdapter;
use Webcreate\Vcs\Git;
use Webcreate\Vcs\Git\Parser\CliParser;

class GIT_webcreate implements I_VCS
{
    private $clone_path;

    // TODO: move in qoss_config file
    const TIMEOUT = 1200;

    /**
     * GIT_webcreate constructor.
     */
    public function __construct()
    {
        $this->clone_path = Configure::read('Clone_path');
    }


    /**
     * See I_VCS documentation
     *
     * @param $repositoryURL
     * @param $repositoryName
     * @return ResponseData
     */
    public function cloneRepository($repositoryURL, $repositoryName)
    {
        try {
            $repository_clone_path = $this->clone_path . '/' . $repositoryName;

            if (!is_dir($repository_clone_path)) {

                $cli = new Cli();
                $cli->setTimeout(self::TIMEOUT);
                $adapter = new CliAdapter('/usr/bin/git', $cli, new CliParser());

                $git = new Git($repositoryURL, $adapter, $repository_clone_path);

                $git->cloneRepository($repository_clone_path);

                return new ResponseData(true, 'Clone ' . $repositoryName . ' successfully.', $repository_clone_path);
            } else {
                return new ResponseData(false, 'Clone ' . $repositoryName . ' failed. Directory name already exist.', $repositoryName);
            }
        } catch (\Exception $e) {
            return new ResponseData(false, 'Clone ' . $repositoryName . ' failed with exception.', [$repositoryName, $e]);
        }

    }

    /**
     * See I_VCS documentation
     *
     * @param $repositoryURL
     * @param $repositoryName
     * @return ResponseData
     */
    public function fetchRepository($repositoryURL, $repositoryName)
    {
        if (is_dir($repository_clone_path = $this->clone_path . '/' . $repositoryName)) {
            $git = new Git($repository_clone_path);
            $git->fetch('origin');

            return new ResponseData(true, 'Fetch ' . $repositoryName . ' successfully.', $repository_clone_path);
        } else {
            return $this->cloneRepository($repositoryURL, $repositoryName);
        }
    }

    /**
     * See I_VCS documentation
     *
     * @param $repositoryPathOrUrl
     * @return ResponseData
     */
    public function getLogs($repositoryPathOrUrl)
    {
        try {
            $git = new Git($repositoryPathOrUrl);
            $logs = $git->log('.');

            $data = array();
            foreach ($logs as $commit) {
                array_push($data,
                    [
                        'date' => $commit->getDate(),
                        'author' => $commit->getAuthor(),
                    ]
                );
            }

            if (!empty($data)) {
                return new ResponseData(true, 'Logs successfully retrieved from ' . $repositoryPathOrUrl, $data);
            } else {
                return new ResponseData(false, 'Logs array is empty : ' . $repositoryPathOrUrl);
            }
        } catch (\Exception $e) {
            return new ResponseData(false, 'Failed to retrieve logs with exception', [$repositoryPathOrUrl, $e]);
        }
    }
}