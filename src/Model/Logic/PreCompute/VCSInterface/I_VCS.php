<?php
/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 24/04/17
 * Time: 09:57
 */

namespace App\Model\Logic\PreCompute\VCSInterface;

use App\Model\Logic\ResponseData;

interface I_VCS
{
    /**
     * Clone the repository of the given $repositoryURL parameter
     * in the $repositoryName folder at the clone_path path,
     * specified in the qoss_config.php
     *
     * @param $repositoryURL , url of the repository to clone/checkout
     * @param $repositoryName , name of the repository, used to name the clone directory
     * @return ResponseData $repository_path, the path of the directory of the cloned repository
     */
    public function cloneRepository($repositoryURL, $repositoryName);

    /**
     * Update the repository to it's latest version.
     * If the path (clone_path parameter in the qoss_config.php file + $repositoryName)
     * to a git repository doesn't exist, it's cloned to the supposed path.
     *
     * @param $repositoryURL , url of the repository to fetch/update
     * @param $repositoryName , name of the repository, used to name the clone directory
     * or find the path to the directory
     * @return ResponseData $repository_path, the path of the directory of the cloned repository
     */
    public function fetchRepository($repositoryURL, $repositoryName);

    /**
     * Extract the log of the given repository URL (svn) or Path (git)
     *
     * @param $repositoryPathOrUrl , String of the path or url of the repository
     * @return array $data, the logs of the given repository path/url.
     * The array contain each commit (iterable with foreach)
     * with their respective date and author as key => value.
     */
    public function getLogs($repositoryPathOrUrl);
}