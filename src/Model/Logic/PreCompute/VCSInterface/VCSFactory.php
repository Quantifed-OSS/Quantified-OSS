<?php
/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 24/04/17
 * Time: 09:56
 */

namespace App\Model\Logic\PreCompute\VCSInterface;

class VCSFactory
{
    const GIT = 'git';
    const SVN = 'svn';
    const MOCK = 'mock';

    private static $instance = null;

    /**
     * @param String $vcsName the name of the vcs the factory need to create.
     * @return GIT_webcreate if $vcsName == 'git'
     * | SVN_webcreate if $vcsName == 'svn'
     * | null if none correspond.
     */
    public function createVcs(String $vcsName)
    {
        if ($vcsName == self::GIT) {
            $vcs = $this->createGit();
        } elseif ($vcsName == self::SVN) {
            $vcs = $this->createSvn();
        } elseif ($vcsName == self::MOCK) {
            $vcs = $this->createMock();
        } else {
            $vcs = null;
        }
        return $vcs;
    }

    /**
     * Singleton
     * @return VCSFactory|null
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new VCSFactory();
        }
        return self::$instance;
    }

    /**
     * @return GIT_webcreate
     */
    private function createGit()
    {
        return new GIT_webcreate();
    }

    /**
     * @return SVN_webcreate
     */
    private function createSvn()
    {
        return new SVN_webcreate();
    }

    /**
     * @return VCS_Mock
     */
    private function createMock()
    {
        return new VCS_Mock();
    }
}