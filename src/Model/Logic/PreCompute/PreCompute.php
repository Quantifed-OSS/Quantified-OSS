<?php

namespace App\Model\Logic\PreCompute;

use App\Model\Entity\Repository;

use App\Model\Logic\PreCompute\Logs\LogsReader;
use App\Model\Logic\PreCompute\Updater\UpdateNotStockedGit;
use App\Model\Logic\PreCompute\Updater\Updater;
use App\Model\Logic\PreCompute\Updater\UpdateStockedGit;
use App\Model\Logic\PreCompute\Updater\UpdateSvn;
use Cake\ORM\TableRegistry;
use App\Model\Logic\ResponseData;


/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 25/04/17
 * Time: 14:59
 *
 * \App\Model\Behavior\LogicCRUDSimplifierBehavior
 * @property \Cake\ORM\Table $Repositories
 * @property Updater Updater
 * @property \Cake\ORM\Table $RawInitialValues
 */
class PreCompute
{

    private $Repositories;

    /**
     * @var \App\Model\Behavior\LogicCRUDSimplifierBehavior $RawInitialValues
     */
    private $RawInitialValues;

    public function __construct()
    {
        $this->Repositories = TableRegistry::get('Repositories');
        $this->RawInitialValues = TableRegistry::get('RawInitialValues');
    }


    /**
     * Execute the PreCompute task on all the Repositories contained in the database.
     *
     * @return array of ResponseData
     */
    public function launchPreComputeOnAllRepositories()
    {
        $repositories = $this->getRepositories();

        $completedPreCompute = array();

        foreach ($repositories as $repository) {
            array_push($completedPreCompute, $this->launchPreCompute($repository));
        }

        return $completedPreCompute;
    }

    /**
     * Execute the PreCompute task :
     * - Get the logs array from the repository object given in parameter
     * - Get the raw_initial_values from the log
     * - And add those value to the raw_initial_values table.
     *
     * @param Repository $repository , the repository to compute data on.
     * @return ResponseData $result, true if action have been perform, else false.
     */
    public function launchPreCompute(Repository $repository)
    {
        if (($logsResponse = $this->getLogsFromRepository($repository))->isSuccess()) {
            $raw_initial_values = LogsReader::getRawInitialValues($logsResponse->getObject());
            $result = $this->insertRIVintoDB($raw_initial_values, $repository);
        } else {
            $result = $logsResponse;
        }

        return $result;
    }

    /**
     * Extract the log of the repository given in parameter.
     *
     * @param Repository $repository
     * @return ResponseData $logsResponse, a ReponseData with an array that contain each commit
     * with the respective date and author as object.
     */
    protected function getLogsFromRepository(Repository $repository)
    {
        $strategy = $this->strategyManager($repository);
        $this->Updater = new Updater($strategy, $repository);
        $logsResponse = $this->Updater->updateRepository();
        return $logsResponse;
    }


    // TODO: refactor into factory ?
    /**
     * Choose the strategy to create in function of the type_vcs and is_stored variable
     * of the Repository object parameter.
     *
     * @param Repository $repository
     * @return UpdateNotStockedGit if type_vcs == 'git' and is_stored == false
     * |UpdateStockedGit if type_vcs == 'git' and is_stored == true
     * |UpdateSvn if type_vcs == 'svn'
     * |null if condition unmet
     */
    private function strategyManager(Repository $repository)
    {
        if ($repository->type_vcs == 'git') {
            if ($repository->is_stored) {
                $strategy = new UpdateStockedGit();
            } else {
                $strategy = new UpdateNotStockedGit();
            }
        } elseif ($repository->type_vcs == 'svn') {
            $strategy = new UpdateSvn();
        } elseif ($repository->type_vcs == 'mock') {
            $strategy = new UpdateStockedGit();
        } else {
            $strategy = null;
        }
        return $strategy;
    }

    /**
     * Insert the value in the database table raw_initial_values
     *
     * @param array $raw_initial_values , values that can be added to the raw_initial_values table
     * @param ResponseData , the repository concerned by the $raw_initial_values
     */
    private function insertRIVintoDB(array $raw_initial_values, Repository $repository)
    {
        $raw_initial_values['repository_id'] = $repository->id;
        return $this->RawInitialValues->add($raw_initial_values);
    }

    /**
     * Find all repositories
     *
     * @return array which contain all the repository in the database table repositories
     */
    private function getRepositories(): array
    {
        return $this->Repositories->find('all')->toArray();
    }
}