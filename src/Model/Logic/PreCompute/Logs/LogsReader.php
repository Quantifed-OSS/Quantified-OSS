<?php

namespace App\Model\Logic\PreCompute\Logs;

use Cake\I18n\Time;

/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 25/04/17
 * Time: 10:23
 */
class LogsReader
{
    private $logs;

    /**
     * LogsReader constructor.
     * Sort the $logs parameter by date by the most recent to the most ancient and
     * initiate the $this->$logs.
     *
     * @param $logs , an array with each commit of a Repository.
     * Each commit have it date and author with corresponding key.
     */
    public function __construct($logs)
    {
        $this->logs = $this->sortLogsByDateDESC($logs);
    }

    //TODO: Maybe not as smart as it might looks and should be refactored as a singleton or a full static class
    /**
     * This function is used a facade to use the LogReader without having
     * to instantiate it nor using a singleton.
     *
     * This function instantiate the LogReader object that will be use to
     * get the raw_initial_values of the given $logs in parameter.
     * Then execute the function to get the raw_initial_values and return them.
     *
     * @param $logs , an array with each commit of a Repository.
     * Each commit have it date and author with corresponding key
     * @return array, all the raw_initial_values of the $logs
     * with their corresponding name as key.
     */
    public static function getRawInitialValues($logs)
    {
        $LR = new self($logs);
        return $LR->extractRawInitialValues();
    }

    /**
     * Execute every function to get each raw_initial_values of the $logs and
     * put each of them in an array with their corresponding name as key.
     *
     * @return array, all the raw_initial_values of the $logs
     * with their corresponding name as key.
     */
    private function extractRawInitialValues()
    {
        $raw_initial_values = [
            'riv_nb_commit_1m' => $this->get_riv_nb_commit_1m(),
            'riv_nb_commit_12m' => $this->get_riv_nb_commit_12m(),
            'riv_nb_contributors' => $this->get_riv_nb_contributors(),
            'riv_total_commit' => $this->get_riv_total_commit(),
            'riv_nb_commit_highest_committer' => $this->get_riv_nb_commit_highest_committer(),
            'riv_last_commit_date' => $this->get_last_commit_date(),
            'riv_first_commit_date' => $this->get_first_commit_date(),
        ];

        return $raw_initial_values;
    }

    /**
     * Count the number of commit in the last 30 days then return it's number.
     *
     * @return int, the number of commit in the last 30 days.
     */
    private function get_riv_nb_commit_1m()
    {
        return count($this->getCommitWithinLast($this->logs, 30));
    }

    /**
     * Count the number of commit in the last 365 days then return it's number.
     *
     * @return int, the number of commit in the last 365 days.
     */
    private function get_riv_nb_commit_12m()
    {
        return count($this->getCommitWithinLast($this->logs, 365));
    }

    /**
     * Count the number of contributors in the given logs and return it.
     *
     * @return int, the number of distinct author (contributors) name.
     */
    private function get_riv_nb_contributors()
    {
        $rawData = $this->logs;
        $rawDataAuthor = $this->formatLogsWithAuthorOnly($rawData);
        return $this->countDistinctContributors($rawDataAuthor);
    }

    /**
     * Count the number of row in the logs array to get the total numbers of commit
     * and return it.
     *
     * @return int, the total number of commit
     */
    private function get_riv_total_commit()
    {
        return count($this->logs);
    }

    /**
     * Sort the log array per distinct author and count the number of row
     * that each distinct author have. Return the highest number of row
     * that an author have as the highest number of commit a distinct author (committer) have.
     *
     * @return int, the number of commit of the committer that have the highest commit number
     */
    private function get_riv_nb_commit_highest_committer()
    {
        $rawDataAuthor = $this->formatLogsWithAuthorOnly($this->logs);
        $nb_commit_per_contributor = array_count_values($rawDataAuthor);
        asort($nb_commit_per_contributor);
        return end($nb_commit_per_contributor);
    }

    /**
     * Get the most recent date amongst each commit and return it in cake Time format
     *
     * @return Time, last commit date, the date of the most recent commit.
     */
    private function get_last_commit_date()
    {
        return $this->logs[0]['date'];
    }

    /**
     * Get the most ancient date amongst each commit and return it in cake Time format
     *
     * @return Time, the first commit date, the date of the most ancient commit.
     */
    private function get_first_commit_date()
    {
        return end($this->logs)['date'];
    }

    /**
     * Return an array that contain all the commit with a date within the last $nb_day days.
     *
     * @param $logsValues , the logs with each commit and their respective date
     * @param $nb_day , the number of days from the current date to select commit from
     * @return array, the commit with their respective information within the last $nb_day
     */
    private function getCommitWithinLast($logsValues, $nb_day)
    {
        $commitWithinLast = array();
        foreach ($logsValues as $commit) {
            $commitDate = new Time($commit['date']);
            if ($commitDate->wasWithinLast($nb_day)) {
                array_push($commitWithinLast, $commit);
            }
        }
        return $commitWithinLast;
    }

    /**
     * Return a new array with all the authors (not distinct) of the $logs parameter
     *
     * @param $logs , an array with each commit and their respective 'author'
     * @return array, all the authors (not distinct) of the $logs
     */
    private function formatLogsWithAuthorOnly($logs)
    {
        $rawDataAuthor = array();
        foreach ($logs as $commit) {
            array_push($rawDataAuthor, $commit['author']);
        }
        return $rawDataAuthor;
    }

    /**
     * Count the number of distinct value from a given array and return it.
     *
     * @param $rawDataAuthor , all the authors(contributors) in an array
     * @return int, the numbers of distinct authors (contributors).
     */
    private function countDistinctContributors($rawDataAuthor)
    {
        $all_contributors = array_unique($rawDataAuthor);
        $raw_value = count($all_contributors);
        return $raw_value;
    }


    /**
     * Sort all commit of the logs by the most recent date
     * to the least recent date of each commit.
     *
     * @param $logs
     * @return mixed
     */
    private function sortLogsByDateDESC($logs)
    {
        usort($logs, function ($a, $b) {
            $t1 = strtotime($a['date']->format('Y-m-d H:i'));
            $t2 = strtotime($b['date']->format('Y-m-d H:i'));
            return $t2 - $t1;
        });
        return $logs;
    }

}