<?php

namespace App\Model\Logic\Compute;

use Cake\ORM\TableRegistry;
use App\Model\Logic\ResponseData;


/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 02/05/17
 * Time: 10:12
 */
class Compute
{
    private $RawFinalValues;
    private $Marks;
    private $Repositories;

    public function __construct()
    {
        $this->RawFinalValues = TableRegistry::get('RawFinalValues');
        $this->Marks = TableRegistry::get('Marks');
        $this->Repositories = TableRegistry::get('Repositories');;
    }

    /**
     * Launch Compute task on every Repositories contained in the data base.
     *
     * @return array
     */
    public function launchComputeOnAllRepositories()
    {
        $repositories = $this->getRepositories();

        $completedCompute = array();

        foreach ($repositories as $repository) {
            array_push($completedCompute, $this->launchCompute($repository));
        }
        return $completedCompute;
    }

    /**
     * Execute Compute task :
     * - compute raw_final_values and marks with raw_initial_values and averages
     * - insert them into the database
     *
     * @param $repository
     * @return ResponseData $computeResult
     */
    public function launchCompute($repository)
    {
        if (($responseData = Calculator::computeRFVAndMarks($repository['id']))->isSuccess()) {
            $responseResult = $this->insertIntoDB($responseData->getObject());
        } else {
            $responseResult = $responseData;
        }
        return $responseResult;
    }

    /**
     * Insert the $data into raw_final_values and marks tables
     *
     * @param array $data , an array with the raw_final_values under the key 'raw_final_value'
     * and marks under the key 'marks'
     * @return
     */
    private function insertIntoDB(array $data)
    {
        $rfvEntity = $this->RawFinalValues->add($data['raw_final_values']);
        $data['marks']['raw_final_value_id'] = $rfvEntity->getObject()->id;

        $markEntity = $this->Marks->add($data['marks']);

        if ($rfvEntity->isSuccess() && $markEntity->isSuccess()) {
            return new ResponseData(true, "Marks and RFV save success", [$rfvEntity, $markEntity]);
        } else {
            return new ResponseData(false, "Marks or RFV save failed", [$rfvEntity, $markEntity]);
        }
    }

    /**
     * Return all the repositories of the Table Repositories
     *
     * @return array, all the repository
     */
    private function getRepositories(): array
    {
        return $this->Repositories->find()->enableHydration(false)->toArray();
    }
}