<?php

namespace App\Model\Logic\Compute;

use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use App\Model\Logic\ResponseData;


/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 02/05/17
 * Time: 14:14
 */
class Calculator
{
    const ROUND_PRECISION = 3;                      // Round precision of calculated values.
    const RFV_RIV_DAY_NUMBER_SELECTION = 30;        // RIV day range selection for RFV calculation

    private $RawInitialValues;
    private $Averages;

    private $riv_nb_contributors;
    private $riv_nb_commit_1m;
    private $riv_nb_commit_12m;
    private $riv_nb_total_commit;
    private $riv_nb_commit_highest_committer;
    private $riv_last_commit_date;
    private $riv_first_commit_date;

    private $average_id;
    private $raw_initial_value_id;
    private $repository_id;

    private $average_commit_1m;
    private $average_commit_12m;

    private $values_initiated = false;

    /**
     * Calculator constructor.
     * Initiate the repository_id with the repository_id parameter then
     * verify if initRIV() and initAverage() return both true. In this case, put values_initiated variable to true.
     * If not, do nothing.
     *
     * @param $repository_id
     */
    private function __construct($repository_id)
    {
        $this->repository_id = $repository_id;

        $this->RawInitialValues = TableRegistry::get('RawInitialValues');
        $this->Averages = TableRegistry::get('Averages');

        if ($this->initRIV() && $this->initAverage()) {
            $this->values_initiated = true;
        }
    }

    /**
     * Create an instance of it's own class and if the values_initiated variable is true,
     * compute all raw_final_values and marks. Else false.
     *
     * @param $repository_id , the id of the repository to compute values on
     * @return ResponseData
     */
    public static function computeRFVAndMarks($repository_id)
    {
        $calculator = new self($repository_id);
        if ($calculator->values_initiated) {
            $computeData = $calculator->calculateRawFinalValuesAndMarks();
            return new ResponseData(true, 'Computing success', $computeData);
        } else {
            return new ResponseData(false, 'Failed to initiate RIV to calculate RFV. Computing failed.', $repository_id);
        }
    }

    /**
     * Select the all raw_initial_values needed and
     * if not null set all raw_initial_values variables of the object
     *
     * @return true if selectLastRIV is not null, else false
     */
    private function initRIV()
    {
        $riv = $this->selectLastRIV();

        if ($riv != null) {
            $this->raw_initial_value_id = $riv['id'];
            $this->riv_first_commit_date = $riv['riv_first_commit_date'];
            $this->riv_last_commit_date = $riv['riv_last_commit_date'];
            $this->riv_nb_commit_1m = $riv['riv_nb_commit_1m'];
            $this->riv_nb_commit_12m = $riv['riv_nb_commit_12m'];
            $this->riv_nb_commit_highest_committer = $riv['riv_nb_commit_highest_committer'];
            $this->riv_nb_contributors = $riv['riv_nb_contributors'];
            $this->riv_nb_total_commit = $riv['riv_total_commit'];

            return true;
        } else {
            return false;
        }
    }

    /**
     * Initiate average related values (average_id, average_commit_1m, average_commit_12m)
     * with the values return by selectLastAverages().
     * Return true if done (selectLastAverage() returned values arn't null), else false.
     *
     * @return bool, true if selectLastAverages() isn't null, else false
     */
    private function initAverage()
    {
        $averages = $this->selectLastAverages();

        if ($averages != null) {
            $this->average_id = $averages['id'];
            $this->average_commit_1m = $averages['average_commit_1m'];
            $this->average_commit_12m = $averages['average_commit_12m'];

            return true;
        } else {
            return false;
        }
    }

    /**
     * Select the most recently created averages table values and return it.
     *
     * @return mixed, last created averages
     */
    private function selectLastAverages()
    {
        $query = $this->Averages->find('averageOrderDESC');

        $result = $query->enableHydration(false)->toArray();
        return $this->getFirstRow($result);
    }

    /**
     * The most recently created RIV of the raw_initial_values table
     * of the $this->repository_id of the current instance
     * Only select RIV if it's created date it within the last 30 days
     *
     * @return mixed last raw_initial_values of the current repository_id variable within 30 days.
     */
    private function selectLastRIV()
    {
        $query = $this->RawInitialValues->find('rIVByRepositoryWithinLast',
            [
                'days' => self::RFV_RIV_DAY_NUMBER_SELECTION,
                'repository_id' => $this->repository_id
            ]
        );

        $result = $query->enableHydration(false)->toArray();
        return $this->getFirstRow($result);
    }

    /**
     * Get all the RFV and marks and return them with niceFormatArray()
     *
     * @return array niceFormatArray (see niceFormatArray() documentation for more information) of RFV and marks.
     */
    private function calculateRawFinalValuesAndMarks()
    {
        return $this->niceFormatArray
        (
            $this->get_last_commit_age(),
            $this->get_project_age(),
            $this->get_nb_contributors(),
            $this->get_delta_commit_1m(),
            $this->get_delta_commit_12m(),
            $this->get_highest_committer_percent()
        );
    }

    /**
     * Get the last commit age and compute it's indicator mark.
     *
     * @return array with the raw_final_value and the mark
     */
    private function get_last_commit_age()
    {
        $lastCommitDate = new Time($this->riv_last_commit_date);

        if ($lastCommitDate->wasWithinLast(30)) {
            $mark = 3;
        } elseif ($lastCommitDate->wasWithinLast(90)) {
            $mark = 2;
        } elseif ($lastCommitDate->wasWithinLast(365)) {
            $mark = 1;
        } else {
            $mark = 0;
        }

        $raw_final_value = $lastCommitDate->diffInDays(new Time('now'));

        return $this->createIndicatorDataArray($mark, $raw_final_value);
    }

    /**
     * @return array with the raw_final_value and the mark
     */
    private function get_project_age()
    {
        $firstCommitDate = new Time($this->riv_first_commit_date);

        if ($firstCommitDate->wasWithinLast(1 * 365)) {
            $mark = 0;
        } elseif ($firstCommitDate->wasWithinLast(3 * 365)) {
            $mark = 1;
        } elseif ($firstCommitDate->wasWithinLast(5 * 365)) {
            $mark = 2;
        } else {
            $mark = 3;
        }

        $raw_final_value = $firstCommitDate->diffInDays(new Time('now'));

        return $this->createIndicatorDataArray($mark, $raw_final_value);
    }

    /**
     * @return array with the raw_final_value and the mark
     */
    private function get_nb_contributors()
    {
        $raw_final_value = $this->riv_nb_contributors;

        if ($raw_final_value > 20) {
            $mark = 3;
        } elseif ($raw_final_value > 13) {
            $mark = 2;
        } elseif ($raw_final_value > 5) {
            $mark = 1;
        } else {
            $mark = 0;
        }

        return $this->createIndicatorDataArray($mark, $raw_final_value);
    }

    /**
     * @return array with the raw_final_value and the mark
     */
    private function get_delta_commit_1m()
    {
        $raw_final_value = round($this->riv_nb_commit_1m / $this->average_commit_1m,
            self::ROUND_PRECISION);

        if ($raw_final_value > 1.5) {
            $mark = 3;
        } elseif ($raw_final_value > 1.25) {
            $mark = 2;
        } elseif ($raw_final_value > 0.75) {
            $mark = 1;
        } else {
            $mark = 0;
        }

        return $this->createIndicatorDataArray($mark, $raw_final_value);
    }

    /**
     * @return array with the raw_final_value and the mark
     */
    private function get_delta_commit_12m()
    {
        $raw_final_value = round($this->riv_nb_commit_12m / $this->average_commit_12m,
            self::ROUND_PRECISION);

        if ($raw_final_value > 1.5) {
            $mark = 3;
        } elseif ($raw_final_value > 1.25) {
            $mark = 2;
        } elseif ($raw_final_value > 0.75) {
            $mark = 1;
        } else {
            $mark = 0;
        }

        return $this->createIndicatorDataArray($mark, $raw_final_value);
    }

    /**
     * @return array with the raw_final_value and the mark
     */
    private function get_highest_committer_percent()
    {
        $raw_final_value = round($this->riv_nb_commit_highest_committer / $this->riv_nb_total_commit,
            self::ROUND_PRECISION);

        if ($raw_final_value <= 0.3) {
            $mark = 3;
        } elseif ($raw_final_value <= 0.6) {
            $mark = 2;
        } elseif ($raw_final_value <= 0.9) {
            $mark = 1;
        } else {
            $mark = 0;
        }

        return $this->createIndicatorDataArray($mark, $raw_final_value);
    }

    /**
     * Return new array with both parameters with respective key name
     *
     * @param $mark
     * @param $raw_final_value
     * @return array with 'rfv' key for $raw_final_value parameter and the 'mark' key for the $mark parameter.
     */
    private function createIndicatorDataArray($mark, $raw_final_value)
    {
        return [
            'rfv' => $raw_final_value,
            'mark' => $mark
        ];
    }

    /**
     * @param array $array
     * @return mixed  : The first row of the given array in parameter
     */
    private function getFirstRow(array $array)
    {
        reset($array);
        return current($array);
    }

    /**
     * Each parameter must have a 'rfv' and 'mark' key
     * @param $last_commit_age
     * @param $project_age
     * @param $nb_contributors
     * @param $delta_commit_1m
     * @param $delta_commit_12m
     * @param $highest_committer_percent
     * @return array : An array with a 'raw_final_values' and 'marks' key where each contain an array
     * with all the key => value needed to respectively add them with the Cakephp ORM to their database tables.
     */
    private function niceFormatArray($last_commit_age, $project_age, $nb_contributors, $delta_commit_1m, $delta_commit_12m, $highest_committer_percent): array
    {
        $niceFormatArray = [
            'raw_final_values' => [
                'rfv_last_commit_age' => $last_commit_age['rfv'],
                'rfv_project_age' => $project_age['rfv'],
                'rfv_nb_contributors' => $nb_contributors['rfv'],
                'rfv_delta_commit_1m' => $delta_commit_1m['rfv'],
                'rfv_delta_commit_12m' => $delta_commit_12m['rfv'],
                'rfv_highest_committer_percent' => $highest_committer_percent['rfv'],
                'average_id' => $this->average_id,
                'repository_id' => $this->repository_id,
                'raw_initial_value_id' => $this->raw_initial_value_id,
            ],
            'marks' => [
                'm_last_commit_age' => $last_commit_age['mark'],
                'm_project_age' => $project_age['mark'],
                'm_nb_contributors' => $nb_contributors['mark'],
                'm_delta_commit_1m' => $delta_commit_1m['mark'],
                'm_delta_commit_12m' => $delta_commit_12m['mark'],
                'm_highest_committer_percent' => $highest_committer_percent['mark'],
                'repository_id' => $this->repository_id,
            ]
        ];
        return $niceFormatArray;
    }
}