<?php

namespace App\Model\Logic\Divisor;

use App\Model\Logic\ResponseData;
use Cake\ORM\TableRegistry;

/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 27/04/17
 * Time: 11:40
 * @property \Cake\ORM\Table RawInitialValues
 * @property \Cake\ORM\Table Repositories
 * @property \Cake\ORM\Table Averages
 */
class Divisor
{
    //TODO: move into qoss_config.php file
    const ELIGIBLE_PROJECT_DAY_NUMBER = 30; // Number of days chosen to determinate if a project is selectable of not.

    private $Averages;
    private $Repositories;

    public function __construct()
    {
        $this->Averages = TableRegistry::get('Averages');
        $this->Repositories = TableRegistry::get('Repositories');
    }

    /**
     * @action
     * - Select the RawInitialValues that will be used to compute averages.
     * - Compute averages then store the result into the database.
     *
     * @return ResponseData
     */
    public function launchDivisor()
    {
        $data = $this->selectRawInitialValuesWithinLast(self::ELIGIBLE_PROJECT_DAY_NUMBER);
        $averages_data = $this->computeAverage($data);
        return $this->Averages->add($averages_data);
    }

    /**
     * @param $dayNumber number of day the finder will use to select the RawInitialValues
     * @return array :
     *   'repository' =>
     *      'raw_initial_values' =>
     *          'number' =>
     *              'riv_nb_commit_1m' => $value_riv_nb_commit_1m
     *              'riv_nb_commit_12m' => $value_riv_nb_commit_12m
     */
    private function selectRawInitialValuesWithinLast($dayNumber)
    {
        $query = $this->Repositories->find('rIVPerRepertoriesWithinLast', ['days' => $dayNumber]);
        return $query->enableHydration(false)->toArray();
    }

    /**
     * @param array $data : An array the contain the RawInitialValues
     * needed to calculate the averages.
     *
     * The array must have the following key structure :
     * 'repository' =>
     *      'raw_initial_values' =>
     *          'row_number' =>
     *              'riv_nb_commit_1m'
     *              'riv_nb_commit_12m'
     *
     * @return array with the averages of 1 month and 12 month of commit by the number of repositories
     * given in $data parameter
     */
    private function computeAverage($data): array
    {
        list($projectCommitSum_1m, $projectCommitSum_12m, $projectNumber) = $this->getAveragesValues($data);

        $average_1m = $this->doAverage($projectCommitSum_1m, $projectNumber);
        $average_12m = $this->doAverage($projectCommitSum_12m, $projectNumber);

        $averages_data = $this->formatResult($average_1m, $average_12m);

        return $averages_data;
    }

    /**
     * @param $numerator
     * @param $denominator
     * @return float|int, value of the numerator divided by the denominator.
     */
    private function doAverage($numerator, $denominator)
    {
        return $numerator / $denominator;
    }

    /**
     * Create an array with key => value :
     * - 'average_commit_1m' => $average_1m,
     * - 'average_commit_12m' => $average_12m
     * and return it
     *
     * @param $average_1m , average commit number for 1m
     * @param $average_12m , average commit number for 12m
     * @return array with :
     * - 'average_commit_1m' as key for param $average_1m and
     * - 'average_commit_12m' as key for param $average_12m.
     */
    private function formatResult($average_1m, $average_12m): array
    {
        $averages_data = [
            'average_commit_1m' => $average_1m,
            'average_commit_12m' => $average_12m
        ];
        return $averages_data;
    }

    /**
     * Get the value to make the averages of 1m commit and 12m commit by project
     *
     * @param array $data , all the project with their number of commit within 1m and their number of commit within 12m
     * @return array with :
     * - $projectNumber, the total number of project
     * - $projectCommitSum_1m, the number of commit withint 1 month of each project
     * - $projectCommitSum_12m, the number of commit withint 12 month of each project
     */
    private function getAveragesValues($data): array
    {
        $projectCommitSum_1m = 0;
        $projectCommitSum_12m = 0;
        $projectNumber = 0;

        foreach ($data as $repository) {
            // key '0' confirm there is indeed at least one RIV.
            if (array_key_exists('0', $repository['raw_initial_values'])) {
                $projectCommitSum_1m += $repository['raw_initial_values'][0]['riv_nb_commit_1m'];
                $projectCommitSum_12m += $repository['raw_initial_values'][0]['riv_nb_commit_12m'];
                $projectNumber++;
            };
        }

        return array($projectCommitSum_1m, $projectCommitSum_12m, $projectNumber);
    }
}