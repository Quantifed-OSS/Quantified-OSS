<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Repository Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property string $name
 * @property string $url
 * @property string $type_vcs
 * @property bool $is_stored
 *
 * @property \App\Model\Entity\Mark[] $marks
 * @property \App\Model\Entity\RawFinalValue[] $raw_final_values
 * @property \App\Model\Entity\RawInitialValue[] $raw_initial_values
 */
class Repository extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
