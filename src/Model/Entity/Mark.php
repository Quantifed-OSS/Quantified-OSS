<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Mark Entity
 *
 * @property int $id
 * @property int $repository_id
 * @property int $raw_final_value_id
 * @property \Cake\I18n\Time $created
 * @property int $m_last_commit_age
 * @property int $m_project_age
 * @property int $m_delta_commit_1m
 * @property int $m_delta_commit_12m
 * @property int $m_nb_contributors
 * @property int $m_highest_committer_percent
 *
 * @property \App\Model\Entity\Repository $repository
 * @property \App\Model\Entity\RawFinalValue $raw_final_value
 */
class Mark extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
