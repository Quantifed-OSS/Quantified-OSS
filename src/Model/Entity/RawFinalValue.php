<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RawFinalValue Entity
 *
 * @property int $id
 * @property int $average_id
 * @property int $raw_initial_value_id
 * @property int $repository_id
 * @property int $rfv_last_commit_age
 * @property int $rfv_project_age
 * @property float $rfv_delta_commit_1m
 * @property float $rfv_delta_commit_12m
 * @property int $rfv_nb_contributors
 * @property float $rfv_highest_committer_percent
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\Average $average
 * @property \App\Model\Entity\RawInitialValue $raw_initial_value
 * @property \App\Model\Entity\Repository $repository
 * @property \App\Model\Entity\Mark[] $marks
 */
class RawFinalValue extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
