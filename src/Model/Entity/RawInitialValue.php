<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RawInitialValue Entity
 *
 * @property int $id
 * @property int $repository_id
 * @property \Cake\I18n\Time $created
 * @property int $riv_nb_commit_1m
 * @property int $riv_nb_commit_12m
 * @property int $riv_nb_contributors
 * @property int $riv_total_commit
 * @property int $riv_nb_commit_highest_committer
 * @property \Cake\I18n\Time $riv_last_commit_date
 * @property \Cake\I18n\Time $riv_first_commit_date
 *
 * @property \App\Model\Entity\Repository $repository
 * @property \App\Model\Entity\RawFinalValue[] $raw_final_values
 */
class RawInitialValue extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
