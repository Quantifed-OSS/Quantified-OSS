<?php

namespace App\Model\Behavior;

use Aura\Intl\Exception;
use Cake\ORM\Behavior;
use App\Model\Logic\ResponseData;

/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 12/04/17
 * Time: 15:25
 */
class LogicCRUDSimplifierBehavior extends Behavior
{
    /**
     * @param $data
     * @return ResponseData
     */
    public function add($data)
    {
        try {
            $table = $this->_table->newEntity($data);
            $table->dirty('modified', true);
            if ($table = $this->_table->save($table)) {
                return new ResponseData(true, 'Save ' . $this->_table->getAlias() . ' success', $table);
            } else {
                return new ResponseData(false, 'Save ' . $this->_table->getAlias() . ' failed.', $data);
            }
        } catch (Exception $e) {
            return new ResponseData(false, 'Found exception in add.', $e);
        }
    }

    public function edit($id, $data)
    {
        $table = $this->_table->get($id);
        $table = $this->_table->patchEntity($table, $data);
        $table = $this->_table->save($table);

        return $table;
    }
}