<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Marks Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Repositories
 * @property \Cake\ORM\Association\BelongsTo $RawFinalValues
 *
 * @method \App\Model\Entity\Mark get($primaryKey, $options = [])
 * @method \App\Model\Entity\Mark newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Mark[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Mark|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Mark patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Mark[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Mark findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MarksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('marks');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LogicCRUDSimplifier');

        $this->belongsTo('Repositories', [
            'foreignKey' => 'repository_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('RawFinalValues', [
            'foreignKey' => 'raw_final_value_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('m_last_commit_age')
            ->allowEmpty('m_last_commit_age');

        $validator
            ->integer('m_project_age')
            ->allowEmpty('m_project_age');

        $validator
            ->integer('m_delta_commit_1m')
            ->allowEmpty('m_delta_commit_1m');

        $validator
            ->integer('m_delta_commit_12m')
            ->allowEmpty('m_delta_commit_12m');

        $validator
            ->integer('m_nb_contributors')
            ->allowEmpty('m_nb_contributors');

        $validator
            ->integer('m_highest_committer_percent')
            ->allowEmpty('m_highest_committer_percent');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['raw_final_value_id'], 'RawFinalValues'));
        $rules->add($rules->existsIn(['repository_id'], 'Repositories'));

        return $rules;
    }
}
