<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RawFinalValues Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Averages
 * @property \Cake\ORM\Association\BelongsTo $RawInitialValues
 * @property \Cake\ORM\Association\BelongsTo $Repositories
 * @property \Cake\ORM\Association\HasMany $Marks
 *
 * @method \App\Model\Entity\RawFinalValue get($primaryKey, $options = [])
 * @method \App\Model\Entity\RawFinalValue newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RawFinalValue[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RawFinalValue|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RawFinalValue patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RawFinalValue[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RawFinalValue findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RawFinalValuesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('raw_final_values');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LogicCRUDSimplifier');

        $this->belongsTo('Averages', [
            'foreignKey' => 'average_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('RawInitialValues', [
            'foreignKey' => 'raw_initial_value_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Repositories', [
            'foreignKey' => 'repository_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Marks', [
            'foreignKey' => 'raw_final_value_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('rfv_last_commit_age')
            ->allowEmpty('rfv_last_commit_age');

        $validator
            ->integer('rfv_project_age')
            ->allowEmpty('rfv_project_age');

        $validator
            ->numeric('rfv_delta_commit_1m')
            ->allowEmpty('rfv_delta_commit_1m');

        $validator
            ->numeric('rfv_delta_commit_12m')
            ->allowEmpty('rfv_delta_commit_12m');

        $validator
            ->integer('rfv_nb_contributors')
            ->allowEmpty('rfv_nb_contributors');

        $validator
            ->numeric('rfv_highest_committer_percent')
            ->allowEmpty('rfv_highest_committer_percent');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['average_id'], 'Averages'));
        $rules->add($rules->existsIn(['raw_initial_value_id'], 'RawInitialValues'));
        $rules->add($rules->existsIn(['repository_id'], 'Repositories'));

        return $rules;
    }


    /**
     * Select the last calculated RFV of the given repository ID given in $option
     * with its RIV, Marks and Averages related.
     *
     * @param Query $q
     * @param array $option
     * @return mixed
     */
    public function findRFVWithRelatedValues(Query $q, array $option)
    {
        return $this->find('all', ['contain' => ['RawInitialValues', 'Averages', 'Marks']])
            ->where(['rawFinalValues.repository_id = ' => $option['repository_id']])
            ->orderDesc('rawFinalValues.created')
            ->first();
    }
}
