<?php
namespace App\Model\Table;

use Cake\I18n\Time;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use DateTime;

/**
 * Repositories Model
 *
 * @property \Cake\ORM\Association\HasMany $Marks
 * @property \Cake\ORM\Association\HasMany $RawFinalValues
 * @property \Cake\ORM\Association\HasMany $RawInitialValues
 *
 * @method \App\Model\Entity\Repository get($primaryKey, $options = [])
 * @method \App\Model\Entity\Repository newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Repository[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Repository|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Repository patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Repository[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Repository findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RepositoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('repositories');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LogicCRUDSimplifier');

        $this->hasMany('Marks', [
            'foreignKey' => 'repository_id'
        ]);
        $this->hasMany('RawFinalValues', [
            'foreignKey' => 'repository_id'
        ]);
        $this->hasMany('RawInitialValues', [
            'foreignKey' => 'repository_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('url')
            ->add('url', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('type_vcs');

        $validator
            ->boolean('is_stored')
            ->allowEmpty('is_stored');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['name']));
        $rules->add($rules->isUnique(['url']));

        return $rules;
    }

    public function findRIVPerRepertoriesWithinLast(Query $query, array $days)
    {
        return $this
            ->find()
            ->select(["id"])
            ->contain(["RawInitialValues" => function (Query $q) use ($days) {
                return $q->select(['repository_id', 'created', 'riv_nb_commit_1m', 'riv_nb_commit_12m'])
                    ->where(['RawInitialValues.created >' => new DateTime('-' . $days['days'] . ' days')])
                    ->order(['RawInitialValues.created DESC']);
            }
            ]);
    }
}
