<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Averages Model
 *
 * @property \Cake\ORM\Association\HasMany $RawFinalValues
 *
 * @method \App\Model\Entity\Average get($primaryKey, $options = [])
 * @method \App\Model\Entity\Average newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Average[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Average|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Average patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Average[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Average findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AveragesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('averages');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LogicCRUDSimplifier');

        $this->hasMany('RawFinalValues', [
            'foreignKey' => 'average_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('average_commit_1m')
            ->allowEmpty('average_commit_1m');

        $validator
            ->numeric('average_commit_12m')
            ->allowEmpty('average_commit_12m');

        return $validator;
    }

    public function findAverageOrderDESC(Query $query)
    {
        return $this
            ->find()
            ->order('Averages.created DESC');
    }
}
