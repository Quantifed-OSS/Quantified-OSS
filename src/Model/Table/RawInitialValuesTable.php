<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use DateTime;

/**
 * RawInitialValues Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Repositories
 * @property \Cake\ORM\Association\HasMany $RawFinalValues
 *
 * @method \App\Model\Entity\RawInitialValue get($primaryKey, $options = [])
 * @method \App\Model\Entity\RawInitialValue newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RawInitialValue[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RawInitialValue|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RawInitialValue patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RawInitialValue[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RawInitialValue findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RawInitialValuesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('raw_initial_values');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LogicCRUDSimplifier');

        $this->belongsTo('Repositories', [
            'foreignKey' => 'repository_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('RawFinalValues', [
            'foreignKey' => 'raw_initial_value_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('riv_nb_commit_1m')
            ->allowEmpty('riv_nb_commit_1m');

        $validator
            ->integer('riv_nb_commit_12m')
            ->allowEmpty('riv_nb_commit_12m');

        $validator
            ->integer('riv_nb_contributors')
            ->allowEmpty('riv_nb_contributors');

        $validator
            ->integer('riv_total_commit')
            ->allowEmpty('riv_total_commit');

        $validator
            ->integer('riv_nb_commit_highest_committer')
            ->allowEmpty('riv_nb_commit_highest_committer');

        $validator
            ->dateTime('riv_last_commit_date')
            ->allowEmpty('riv_last_commit_date');

        $validator
            ->dateTime('riv_first_commit_date')
            ->allowEmpty('riv_first_commit_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['repository_id'], 'Repositories'));

        return $rules;
    }

    public function findRIVByRepositoryWithinLast(Query $query, array $options)
    {
        return $this
            ->find()
            ->where(['RawInitialValues.created >' => new DateTime('-' . $options['days'] . ' days')])
            ->andWhere('RawInitialValues.repository_id = ' . $options['repository_id'])
            ->order(['RawInitialValues.created DESC']);
    }
}
