<?php
/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 11/05/17
 * Time: 10:24
 */

namespace App\Controller\Api\V1;

use App\Controller\AppController;
use App\Model\Logic\Compute\Compute;
use App\Model\Logic\Divisor\Divisor;
use App\Model\Logic\PreCompute\PreCompute;

/**
 * @property \App\Model\Table\RepositoriesTable Repositories
 * @property \App\Model\Logic\PreCompute\PreCompute $PreCompute
 * @property \App\Model\Logic\Compute\Compute $Compute
 * @property \App\Model\Logic\Divisor\Divisor $Divisor
 * @property \App\Model\Table\RawFinalValuesTable RawFinalValues
 */
class APIController extends AppController
{
    private $PreCompute;
    private $Compute;
    private $Divisor;

    /**
     *
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Marks');
        $this->loadModel('Averages');
        $this->loadModel('RawFinalValues');
        $this->loadModel('RawInitialValues');
        $this->loadModel('Repositories');

        $this->PreCompute = new PreCompute();
        $this->Divisor = new Divisor();
        $this->Compute = new Compute();
    }

    /**
     * Add the repositories contained in the request's input to the database
     * then compute the data with PreCompute, Divisor and Compute
     *
     * The request must be of type 'json' and 'post'.
     *
     * Here is an example of how the json must look like.
     *
     *{
     *   "repositories": [
     *      {
     *          "name": "foo",
     *          "url": "https://gitfoo/foo/bar",
     *          "type_vcs": "git",
     *          "is_stored": true
     *      }
     *   ]
     *}
     *
     * HTTP Response is always 200. Repositories that could not be added to the database
     * or that add problem during the computing are returned in the json format
     */
    public function addRepositories()
    {
        if ($this->request->is(['json', 'post'])) {
            $repositories = $this->request->input();
            $convertedToArrayRepositories = json_decode($repositories, true);
            $result = $this->saveAndCompute($convertedToArrayRepositories['repositories']);
        }

        $this->set(compact('result'));
    }

    /**
     * Return the indicators values of the repositories data given in the input request under json format.
     *
     * Input json example
     * {
     *      "repositories":[
     *          {
     *              "name":"repos_name_1"
     *          },
     *          {
     *              "name":"repos_name_2"
     *          }
     *      ]
     * }
     */
    public function getRepositoriesValues()
    {
        if ($this->request->is(['json', 'post'])) {
            $repositoriesName = $this->request->input();
            $convertedToArrayRepositories = json_decode($repositoriesName, true);
            $values = $this->getLastValuesFromRepositories($convertedToArrayRepositories['repositories']);

            $this->set(compact('values'));
        }
    }

    /**
     * Execute startPreCompute(), startDivisor() then startCompute() and set the results to the view.
     */
    public function startComputing()
    {
        $PC = $this->PreCompute->launchPreComputeOnAllRepositories();
        $D = $this->Divisor->launchDivisor();
        $C = $this->Compute->launchComputeOnAllRepositories();

        $failed_PC = $this->selectFailedFromResponseDataArray($PC);
        $failed_D = $this->selectFailedFromResponseDataArray($D);
        $failed_C = $this->selectFailedFromResponseDataArray($C);

        $results = array();
        array_push($results, [$failed_PC, $failed_D, $failed_C]);

        $this->set(compact('results'));
    }

    /**
     * Execute launchPreComputeOnAllRepositories,
     * select the $failed_result then set the $failed_result to the view and return them.
     */
    public function startPreCompute()
    {
        $results = $this->PreCompute->launchPreComputeOnAllRepositories();

        $failed_results = $this->selectFailedFromResponseDataArray($results);

        $this->set(compact('failed_results'));
    }

    /**
     * Execute launchDivisor, set the result to the view and return it's result.
     */
    public function startDivisor()
    {
        $results = $this->Divisor->launchDivisor();

        $this->set(compact('results'));
    }

    /**
     * Execute launchComputeOnAllRepositories,
     * select the $failed_result then set the $failed_result to the view and return them.
     */
    public function startCompute()
    {
        $results = $this->Compute->launchComputeOnAllRepositories();

        $failed_results = $this->selectFailedFromResponseDataArray($results);

        $this->set(compact('failed_results'));
    }

    /**
     * Select the repositories entities of the repositoriesName given in parameter
     * and select the last values of RFV, Marks, RIV and Averages.
     * Return them as en array.
     *
     * @param array $repositoriesName
     * @return array, the result of the query
     */
    private function getLastValuesFromRepositories(array $repositoriesName)
    {
        $result = array();

        foreach ($repositoriesName as $repositoryName) {
            $repos = $this->getRepository($repositoryName['name']);
            $result = $this->getLastValues($repos, $result);
        }

        return $result;
    }

    /**
     * @param array $repositories , an array of repository with their 'name', 'url', 'type_vcs', 'is_stored'
     * as key => value
     * @return array
     */
    private function saveAndCompute(array $repositories)
    {
        $result = array();

        foreach ($repositories as $repository) {
            if (($repositoryResponseData = $this->Repositories->add($repository))->isSuccess()) {
                $PC_results = $this->PreCompute->launchPreCompute($repositoryResponseData->getObject());
                $D_results = $this->Divisor->launchDivisor();
                $C_results = $this->Compute->launchCompute($repositoryResponseData->getObject());

                array_push($result, [
                    'Repository_data' => $repositoryResponseData,
                    'PreCompute' => $PC_results,
                    'Divisor' => $D_results,
                    'Compute' => $C_results,
                ]);
            }
        }

        return $result;
    }

    /**
     * @see findRFVWithRelatedValues
     *
     * @param $repos
     * @param array $result
     * @return array
     */
    private function getLastValues($repos, array $result)
    {
        foreach ($repos as $repository) {
            $rfv = $this->RawFinalValues->find('rFVWithRelatedValues', ['repository_id' => $repository['id']])
                ->toArray();
            array_push($result, $rfv);
        }
        return $result;
    }

    /**
     * Select the repository entity corresponding to the $repositoryName given in parameter.
     *
     * @param $repositoryName
     * @return mixed
     */
    private function getRepository($repositoryName)
    {
        return $this->Repositories->find()->where(['Repositories.name =' => $repositoryName])
            ->toArray();
    }

    /**
     * Select the ResponseData object from the parameter with the success attribute to false.
     *
     * @param $results
     * @return array
     */
    private function selectFailedFromResponseDataArray($results)
    {
        $failed_results = array();
        foreach ($results as $result) {
            if ($result->isNotSuccess()) {
                array_push($failed_results, $result);
            }
        }
        if ($failed_results == null) {
            return $failed_results = ["No problem occured."];
        }
        return $failed_results;
    }
}