<?php
/**
 * Created by PhpStorm.
 * User: yliebart
 * Date: 06/03/17
 * Time: 10:20
 */
namespace App\Error;

use Cake\Error\BaseErrorHandler;

class AppError extends BaseErrorHandler
{

    public function _displayError($error, $debug)
    {
        return 'Il y a eu une erreur!';
    }
    public function _displayException($exception)
    {
        return 'Il y a eu un exception';
    }

    public function handleFatalError($code, $description, $file, $line)
    {
        return 'Une erreur fatale est survenue';
    }
}