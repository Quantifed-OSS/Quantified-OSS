CREATE TABLE public.repositories (
  id        SERIAL        NOT NULL,
  created   TIMESTAMP WITH TIME ZONE DEFAULT NULL,
  name      VARCHAR(128)  NULL,
  url       VARCHAR(1000) NULL,
  type_vcs  VARCHAR(45)   NULL,
  is_stored BOOLEAN       NULL,
  PRIMARY KEY (id),
  CONSTRAINT name_UNIQUE UNIQUE (name),
  CONSTRAINT url_UNIQUE UNIQUE (url)
);

CREATE TABLE public.raw_initial_values (
  id                              SERIAL                   NOT NULL,
  repository_id                   INT                      NOT NULL,
  created                         TIMESTAMP WITH TIME ZONE DEFAULT NULL,
  riv_nb_commit_1m                INT                      NULL,
  riv_nb_commit_12m               INT                      NULL,
  riv_nb_contributors             INT                      NULL,
  riv_total_commit                INT                      NULL,
  riv_nb_commit_highest_committer INT                      NULL,
  riv_last_commit_date            TIMESTAMP WITH TIME ZONE NULL,
  riv_first_commit_date           TIMESTAMP WITH TIME ZONE NULL,
  PRIMARY KEY (id),
  CONSTRAINT repository_riv_fk
  FOREIGN KEY (repository_id)
  REFERENCES public.repositories (id)
  ON DELETE CASCADE
  ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table public.averages
-- -----------------------------------------------------
CREATE TABLE public.averages (
  id                 SERIAL                   NOT NULL,
  created            TIMESTAMP WITH TIME ZONE NULL DEFAULT NULL,
  average_commit_1m  FLOAT                    NULL,
  average_commit_12m FLOAT                    NULL,
  PRIMARY KEY (id)
);

-- -----------------------------------------------------
-- Table public.raw_final_values
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS public.raw_final_values (
  id                            SERIAL                   NOT NULL,
  average_id                    INT                      NOT NULL,
  raw_initial_value_id          INT                      NOT NULL,
  repository_id                 INT                      NOT NULL,
  rfv_last_commit_age           INT                      NULL,
  rfv_project_age               INT                      NULL,
  rfv_delta_commit_1m           FLOAT                    NULL,
  rfv_delta_commit_12m          FLOAT                    NULL,
  rfv_nb_contributors           INT                      NULL,
  rfv_highest_committer_percent FLOAT                    NULL,
  created                       TIMESTAMP WITH TIME ZONE NULL,
  PRIMARY KEY (id),
  CONSTRAINT repository_rfv_fk
  FOREIGN KEY (repository_id)
  REFERENCES public.repositories (id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT riv_rfv_fk
  FOREIGN KEY (raw_initial_value_id)
  REFERENCES public.raw_initial_values (id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT average_riv_fk
  FOREIGN KEY (average_id)
  REFERENCES public.averages (id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table public.marks
-- -----------------------------------------------------
CREATE TABLE public.marks (
  id                          SERIAL                   NOT NULL,
  repository_id               INT                      NOT NULL,
  raw_final_value_id          INT                      NOT NULL,
  created                     TIMESTAMP WITH TIME ZONE NULL DEFAULT NULL,
  m_last_commit_age           INT                      NULL,
  m_project_age               INT                      NULL,
  m_delta_commit_1m           INT                      NULL,
  m_delta_commit_12m          INT                      NULL,
  m_nb_contributors           INT                      NULL,
  m_highest_committer_percent INT                      NULL,
  PRIMARY KEY (id),
  CONSTRAINT raw_final_value_id_UNIQUE UNIQUE (raw_final_value_id),
  CONSTRAINT repository_mark_fk
  FOREIGN KEY (repository_id)
  REFERENCES public.repositories (id)
  ON DELETE CASCADE
  ON UPDATE NO ACTION,
  CONSTRAINT rfv_mark_fk
  FOREIGN KEY (raw_final_value_id)
  REFERENCES public.raw_final_values (id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
