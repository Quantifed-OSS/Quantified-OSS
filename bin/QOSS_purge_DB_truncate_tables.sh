#!/usr/bin/env bash

set -o errexit

# #############################################################################
# Option management
# #############################################################################

TEMP=`getopt -o d: -- "$@"`

if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi

usage () {
    echo "usage: ./${0} [OPTIONS]..."
    echo ''
    echo '  -d <qoss-dir>   (MANDATORY) Absolute directory to Quantified-OSS, *without* trailing slash, eg "/home/qoss/qoss"'
    echo ''
    exit 2
}

# Note the double quotes around $TEMP: they are essential!
eval set -- "${TEMP}"

declare QOSS_DIR=

while true; do
    case "$1" in
        -d ) QOSS_DIR="$2"; shift 2 ;;
        -- ) shift; break ;;
        * ) break ;;
    esac
done

# check mandatory options
if [ "${QOSS_DIR}" = "" ]
then
    echo ''
    echo 'Mandatory option is missing'
    echo ''
    usage
fi

# #############################################################################
# Actual job
# #############################################################################

sudo -u postgres psql -f "${QOSS_DIR}/config/SQL/@@@"
