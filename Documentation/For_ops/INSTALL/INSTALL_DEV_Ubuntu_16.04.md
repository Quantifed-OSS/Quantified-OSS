# Quantified-OSS - Installation guide

## PREREQUISITES: Ubuntu packages

```shell
sudo apt-get install \
    libicu55 \
    zlib1g \
    git \
    apache2 \
    php \
    php-intl \
    libapache2-mod-php \
    postgresql \
    php-pgsql \
    php7.0-sqlite3
```

## PREREQUISITES: PHP > timezone + max_upload

As user `root`, do:

```shell
for i in apache2 cli; do
    echo "date.timezone = \"Europe/Paris\"" >> /etc/php/7.0/${i}/conf.d/comptoir_du_libre.ini
    echo "upload_max_filesize = 2M" >> /etc/php/7.0/${i}/conf.d/comptoir_du_libre.ini
done
```

## PREREQUISITES: Install Composer

As user `root`, do:

```shell
php -r "readfile('https://getcomposer.org/installer');" \
    | php -- --install-dir=/usr/local/bin --filename=composer
```

## PREREQUISITES: Remove useless packages

```shell
sudo apt-get purge --auto-remove \
    libicu55 \
    zlib1g
```

## PREREQUISITES: Postgres authentication

In `/etc/postgresql/9.5/main/pg_hba.conf`, replace

```
       local   all             all                                     peer
```

by

```
       local   all             all                                     password
```

and restart Postgres

```shell
sudo service postgresql restart
```

## PREREQUISITES: Create system user

As user `root`, do:

```shell
useradd -d /home/qoss/ -m -s /bin/bash qoss
```

## PREREQUISITES: Create group

As user `root`, do:

```shell
groupadd qoss
usermod -aG qoss qoss
```

## PREREQUISITES: Create tmp directory

As user `qoss`, do:

```shell
mkdir /home/qoss/tmp/
```

<!-- =============================================================================================================== -->
<!-- ===== Installation ============================================================================================ -->
<!-- =============================================================================================================== -->

## User space setup

Let say:

* we are user `superdupont`,
* we want to have our Quantified-OSS project files into `/home/superdupont/Project/quantified-oss/`,
* we can do `git` command as authenticated user (i.e. ssh key is placed on the Gitlab server)

As user `root`, do:

```shell
usermod -aG qoss superdupont
```

As user `superdupont`, do:

```shell
cd /home/superdupont/Project/
git clone git@gitlab.adullact.net:Quantifed-OSS/Quantified-OSS.git
```

Now configure code for `qoss` user:

```shell
su - qoss
cd /home/qoss
ln -s /home/superdupont/Project/quantified-oss quantified-oss
```

## POSTGRESQL Create user

```shell
sudo -u postgres psql
```

Adjust password to your needs:

```postgresql
CREATE USER qoss WITH PASSWORD 'qoss';
```

## POSTGRESQL Create DB and set ownership

As `root` user, do:

```shell
/home/qoss/quantified-oss/bin/QOSS_create_DB_and_set_ownership.sh -d /home/qoss/quantified-oss
```

Test it:

```shell
psql -U qoss -W qoss
```

## POSTGRESQL Create tables and procedures

As any user, do:

```shell
/home/qoss/quantified-oss/bin/QOSS_create_tables_and_procedures.sh -d /home/qoss/quantified-oss
```

<!-- =============================================================================================================== -->
<!-- ===== Configuration =========================================================================================== -->
<!-- =============================================================================================================== -->

## Composer install

As user `superdupont`, do:

```shell
cd /home/qoss/quantified-oss/ \
&& /usr/local/bin/composer install
```

## Load Debug kit for Comptoir **SRV**

As user `superdupont`, do:

```shell
cd /home/qoss/quantified-oss/ \
&& bin/cake plugin load DebugKit
```

## App.php -> datasources

As user `superdupont`, edit `config/app.php`. In section `Datasource/Default`, set the values for:

* host
* port
* username
* password
* database

## Set UNIX permissions

As user `root` do:

```shell
cd /home/qoss/
chmod -R o-w quantified-oss/*
for i in webroot logs tmp; do
    chown -R www-data.qoss "quantified-oss/${i}"
    find "quantified-oss/$i" -type d -exec sudo chmod 775 {} \;
    find "quantified-oss/$i" -type f -exec sudo chmod 664 {} \;
done
```

## Set tmp directory for git clones

Modify the path in `config/qoss_config.php`

<!-- =============================================================================================================== -->
<!-- ===== Virtual hosts =========================================================================================== -->
<!-- =============================================================================================================== -->

## APACHE Vhost preparation

As `root`, edit `/etc/hosts` and add `qoss.local` next to the "localhost" entry:

```
127.0.0.1	localhost my-computer-name qoss.local
```

## APACHE Vhost creation "qoss"

Create the file `/etc/apache2/sites-available/qoss.conf` and add the following content:

```apacheconfig
<VirtualHost *:80>
    ServerName qoss.local
    ServerAdmin webmaster@localhost
    DocumentRoot /home/qoss/quantified-oss/webroot/

    ErrorLog ${APACHE_LOG_DIR}/qoss.local.log
    CustomLog ${APACHE_LOG_DIR}/qoss.local.log combined

    <Directory "/home/qoss/quantified-oss/webroot/">
        AllowOverride All
    </Directory>

    <Location />
        Require all granted
    </Location>
</VirtualHost>
```

## APACHE enable mod_rewrite and new vhosts

As `root` user, do:

```shell
a2enmod rewrite
a2ensite qoss.conf
service apache2 reload
```

## How to use Quantified

With shell :
```in Quandtified-OSS/
bin/cake QuantifiedLauncher startCompute()
bin/cake QuantifiedLauncher startDivisor()
bin/cake QuantifiedLauncher startPreCompute()
```

You can use these with Cronjobs

With HTTP Request on https://localhost/api/v1/API/ :
* startComputing.json - exec the 3 following : startPreCompute, startDivisor, startCompute and return their result.
* startCompute.json - exec Compute module
* startPreCompute.json - exec PreCompute module
* startDivisor.json - exec Divisor module
* addRepositories.json - returned which actions worked with results
 Need POST request input in json format :
```
  {
     "repositories": [
        {
            "name": "foo",
            "url": "https://gitfoo/foo/bar",
            "type_vcs": "git",
            "is_stored": true
        }
     ]
  }
```
* getRepositoriesValues.json - return the values of each repository given in the http request json.
Need POST resquest input in json format :
```
  {
       "repositories":[
           {
               "name":"repos_name_1"
           },
           {
               "name":"repos_name_2"
           }
       ]
  }
```

To test HTTP request, you can use in PHPStorm Tools>Test RESTful web services.