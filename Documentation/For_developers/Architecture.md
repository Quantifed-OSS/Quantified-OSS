# Quantified-OSS architecture

QUantified-OSS (QOSS en raccourci) est composé des blocs fonctionnels suivants:

* API
* Compute
* Fetch
* List Manager

Un bloc fonctionnel est défini comme une entité ayant une tâche (ou famille de tâches) à accomplir. Il en a la responsabilité.

## API

### Responsabilités

* répondre aux requêtes de l'API REST exposée
* transmettre des ordres aux autres blocs fonctionnnels

### Primitives exposées

#### submitRepos(url)

Input:

* url: string URL du repos (URL Git ou URL SVN)

Output:

* reposId: int, id QOSS du repos

#### getResult(reposId)

Input:

* reposId: int, id QOSS du repos

Output:

If results are not yet computed: a message sort of "please come back later"
Else: the results, i.e.

* a set of:
    * indicatorName: name as stated in [Indicators doc](Indicateurs.md)
    * rawValue
    * finalValue
    * date: date (timestamp ?) of the computation

## Compute

### Responsabilités

* Calculer les valeurs brutes et valeurs finales des indicateurs.
* Le calcul s'effectue en s'appuyant sur une **copie locale** du repos du projet à mesurer.

Notes:

* Les résultats des calculs sont stockés en base de données.
* Seule la dernière et l'avant-dernière valeur sont stockées.

## Fetch

### Responsabilités

* Générer la copie local du repos d'un projet
* Supprimer les copies locales de repos
* Mettre à jour les copies locales de repos (seulement pour les repos présents dans le List Manager, voir plus ci-après)

Remarque:

* les types de repos à gérer sont Git et Svn

## List Manager

### Responsabilités

* Gérer des listes de repos afin de les traiter à des fréquences différentes

Cas typique: disposer d'une liste "Comptoir du Libre" avec les repos des projets du Comptoir


