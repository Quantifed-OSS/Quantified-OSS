# Conception

## Objet du document

Ce document a pour objet de présenter la conception de Quantified-OSS (QOSS). Ce dernier permet de mesurer le niveau de maturité d'un logiciel libre par le calcul de divers indicateurs.

![Schéma d'ensemble QOSS](Images/qoss_ensemble.svg?raw=true)

## Modules de QOSS

QOSS est composé des modules suivants:

* Pre-compute
* Divisor
* Compute

![Schéma des modules de QOSS](Images/qoss_modules.svg?raw=true)

## Types de projets gérés

Un projet est constitué d'une URL pointant vers le code source du projet. Les projets supportés par QOSS s'appuient sur les VCS (Version Control System) suivants : GIT ou SVN.

On distingue deux types de projets :

* ceux présents dans le Comptoir du Libre (on parlera de "Projet Comptoir")
* ceux absents du Comptoir (on parlera de "Projet Extérieur")

## Module Pre-compute

Le module Pre-Compute travaille sur les projets ("repositories" ou repos), ces derniers doivent avoir été préalablement insérés en base de données.

Le module Pre-Compute comporte deux étapes.

### Étape 1

Selon le VCS du projet et son type (Comptoir ou Extérieur), le comportement varie :

* Git Comptoir : mise à jour du repos
* Git Exterieur : clonage du repos
* SVN Comptoir ou Extérieur : par la nature centralisée de SVN, les repos ne sont pas copiés en local.

Les répertoires qui n'ont pas de données ou on des données anciennent sont priviligiés.

### Étape 2

À partir du log d'un repos (svn log ou git log), les données suivantes sont récupérées et stockées en base :
    
- nombre de commits sur les 30 derniers jour (`vib_nb_commit_1m`)
- nombre de commits sur les 365 derniers jour (`vib_nb_commit_12m`)
- nombre de contributeurs (`vib_nb_contributors`)
- données nécessaires pour `highest_commiter_percent` (`vib_highest_commiter_percent`)
- date du dernier commit (`vib_last_commit_date`)
- date du premier (`vib_first_commit_date`)

La date à laquelle ces données ont été calculées est primordiale. Elle est elle aussi stockée en base.

### Gestion des erreurs

Si une erreur survient, elle doit être gérée pour ne pas interrompre la mise à jour ou le calcul.

## Module Divisor

Le module Divisor s'exécute indépendamment des autres modules. Il calcule les moyennes avec les données présentes dans la base de données. Le module Divisor a pour responsabilité de calculer les moyennes suivantes :

* nombre moyen de commits sur 30 jours (utilisé pour l'indiciteur `delta_commit_1m`)
* nombre moyen de commits sur 365 jours (utilisé pour l'indiciteur `delta_commit_12m`)

Cependant notre calcul de moyenne est particulier (en cela le terme "moyenne" est impropre mais nous l'avons choisi malgré tout pour simplifier le vocabulaire). La spécificité de notre calcul de moyenne est d'utiliser des plages de durée identiques mais dont la date de fin peut varier d'un projet à l'autre.

### Étape 1 : sélection de projets éligibles

Les projets éligibles sont ceux dont la date de Pre-Compute est vieille de 30 jours au plus.

C'est-à-dire qu'à un jour J, la date de Pre-Compute la plus vieille utilisée sera de J-30.

### Étape 2 : calcul de la moyenne

La moyenne se calcule de la sorte :

* pour les projets éligibles,
* faire la somme du nombre de commits sur la plage de durée choisie (30j ou 365j),
* diviser cette somme par le nombre de projets éligibles.

Par exemple pour la moyenne sur 30j, cette dernière est calculée avec une plage de durée de 30 jours parmi les 60 derniers jours au moment du calcul. De même pour la moyenne sur 365j, elle est calculée avec une plage de durée de 365 jours parmi les 395 (=365+30) derniers jours au moment du calcul.

### Étape 3 : stockage en base

Les moyennes calculées sont stockées en base, ainsi que leur date de calcul.

### Fréquence de (re)calcul

Les moyennes sont recalculées tous les jours.

## Module Compute

Le module Compute a pour responsabilité de calculer, pour chaque indicateur, sa valeur brute finale et son score (entier compris entre 0 et 3).

Ces dernières sont calculées à partir des valeurs ajoutées en base par les modules Pre-Compute et Divisor.
Les données calculées il y a plus de 30 jours ne sont pas utilisées.
Le résultat des calculs est stocké en base, accompagné de sa date de calcul.

Pour mémoire, les indicateurs sont :

- `delta_commit_1m`
- `delta_commit_12m`
- `nb_contributors`
- `highest_commiter_percent`
- `last_commit_age`
- `project_age`

Le module Compute s'exécute indépendamment des autres modules.

## Fréquence d'exécution des modules

* Module Pre-Compute :
    * Projet Git Comptoir : tous les jours
    * Projet SVN Comptoir : tous les jours
    * Projet Git Exterieur : tous les mois
    * Projet SVN Exterieur : tous les mois
* Module Divisor : tous les jours
* Module Compute :
    * a minima : à la même fréquence que le plus lent des Pre-Compute. Exemple : si le Pre-Compute se fait tous les jours & tous les mois, le Compute doit se faire au moins tous les mois.
    * il peut se faire à une fréquence plus élevée. En suivant l'exemple précédent, il possible de le lancer disons tous les jours.

## Module API

Le module API a plusieurs fonctionnalités :

- Ajouter à la base de données un ou plusieurs dépôts qui n'existent pas
- Envoyer les notes d'un ou plusieurs dépôts demandés

### Scénario d'ajout d'un ou plusieurs dépôts

API ajoute la liste des dépôts demandés dans le cas où ils ne sont pas déjà présents dans la base de données.
Il demande également à ce que les notes des dépôts ajoutés soient calculées.

Si le dépôt doit être stocké en local, le module API se charge de le cloner.

**Cas d'erreurs**

Si le dépôt existe déjà, les autres dépôts sont quand même ajoutés. 
Un message d'erreur affiche qu'elles sont les dépôts qui étaient déjà ajoutés. 

### Scénario du retour des notes d'un ou plusieurs dépôts

Avec les listes des dépôts demandés, le module envoie un fichier sous format `.json` des dernières notes calculées.


