# Liste des indicateurs Quantified-OSS

## Normalisation des résultats

Un indicateur procède à une mesure. La valeur absolue mesurée n'a pas de sens. Elle ne prend du sens que si elle est comparée à d'autres valeurs. Pour cela, nous proposons de normaliser les résultats.

* Un indicateur ne peut renvoyer que des valeurs entières comprises entre 0 et 3.
* Valeur brute : valeur mesurée avant normalisation
* Valeur finale : valeur normalisée de la valeur brute.

## Âge du dernier commit `last_commit_age`

Principe : plus c'est récent, mieux c'est.

La valeur brute est mesurée en jours.

Valeur brute (en jours) | valeur finale
------------------------|--------------
< 30                    | 3
< 90                    | 2
< 365                   | 1
sinon                   | 0

## Âge du projet `project_age`

Âge du projet (pas du repos) = delta entre date du premier et la date de consultation

Principe : plus c'est vieux, mieux c'est.

La valeur brute est mesurée en jours.

Valeur brute (en jours) | valeur finale
------------------------|--------------
> 5 * 365               | 3
> 3 * 365               | 2
> 1 * 365               | 1
sinon                   | 0

## Nombre relatif de commits sur le dernier mois `delta_commit_1m`

* Soit X le nombre de commits sur le dernier mois pour le projet à mesurer.
* Soit T le nombre moyen de commits sur le dernier mois, basé sur tous les projets déjà mesurés.

La valeur brute est un pourcentage et calculé comme suite : X / T

Remarques :

* La valeur brute n'est donc **pas** le nombre de commits, mais bien un pourcentage.
* Le calcul de la valeur brute évolue avec le temps car le nombre de projets mesurés augmente.

Valeur brute | valeur finale
-------------|--------------
> 150%    | 3
> 125%    | 2
> 75%     | 1
sinon     | 0

## Nombre relatif de commits sur les 12 derniers mois `delta_commit_12m`

Idem  `delta_commit_1m` mais sur 12 mois.

Valeur brute | valeur finale
-------------|--------------
> 150%    | 3
> 125%    | 2
> 75%     | 1
sinon     | 0

## Nombre relatif de contributeurs `nb_contributors`

La valeur brute est le nombre de contributeurs.

Remarques :

* Un contributeur est défini comme un email ayant fait un commit.
* Le cas d'une même personne ayant plusieurs email n'est pas géré.

Valeur brute | valeur finale
-------------|--------------
>= 20    | 3
>= 13    | 2
>= 5     | 1
sinon    | 0

## Couverture du code par le fort contributeurs `highest_commiter_percent`

* Soit T le nombre total de commit du projet
* Soit Xi le nombre de commit du contributeur i
* Soit Ci = Xi / T

La valeur brute est le maximum des Ci.

Remarques :

* Ci est un pourcentage

Valeur brute | valeur finale
-------------|--------------
<= 30%   | 3
<= 60%   | 2
<= 90%   | 1
sinon    | 0


