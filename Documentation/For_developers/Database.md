# Base de données

Ce fichier résume les points important à retenir concernant la base de données. Référez-vous au MCD ci-dessous pour avoir une vue d'ensemble de la base de données.

![MCD](Images/database_diagram)

## Choix conceptuels

### Les relations

**Relation One to Many entre `raw_initial_values` et `répositories`**

Cette relation est nécessaire car les données de `raw_initial_values` seront calculées avant celle de `raw_final_values`. Il est donc important de connaitre à ce moment, à quel répertoire ces données appartiennent.

**Relation One to Many entre `marks` et `repositories`**

Cette relation n'est pas nécessaire pour l'instant mais pourrait le devenir dans le futur ou peut simplifier certaines requêtes.

**Relations d'inclusion**

Les relations d'inclusion que les 2 précédentes relations peuvent sucitées non pas été implémentées car nous avons le contrôl de la base de données. Il a donc été choisi pour simplifier l'impletation de s'en passer.

### Disposition des données

**Les indicateurs**

Il est possible de représenter cette base de données en utilisant des héritages.

Cela évite dans le cas où l'on voudrait ajouter un nouvel indicateur de devoir ajouter manuellement dans les tables `raw_initial_values`, `raw_final_values` et `marks` les champs nécessaires pour celui-ci.

 Pour des raisons de clarté et de simplicité d'insertion des données, il a été décidé de mettre de côté ce type de conception.

**Les champs `created`**

Les champs `created` de `raw_final_values` et `marks` non pas raisons d'exister pour l'instant si ce n'est dans la prévision du utilisation futur.