# QOSS v2 - cahier des charges

## Responsabilité

Fournir des indicateurs objectifs / transparents / répétables

## Architecture

URL de repos (Git / SVN) ----> QOSS ----> Liste d'indicateurs avec valeurs

QOSS est appelable par une API Rest

## Technologie & motivations

-> PHP / CakePHP

Ruby ou Python avait été choisis auparavant par la présence de bibliothèque facilitant l'accès aux dépôts Git ou SVN. Il s'avère que des bibliothèques similaires existent en PHP. De plus nous disposons d'une expérience de création d'application CakePHP avec API Rest ; exemple [Comptoir](https://gitlab.adullact.net/Comptoir/) ou [E-Collectivité](https://gitlab.adullact.net/E-Collectivite)

Exemples de bibliothèques PHP pour Git :

* [git-php](https://github.com/czproject/git-php)
* [PHP-Stream-Wrapper-for-Git](https://github.com/teqneers/PHP-Stream-Wrapper-for-Git)

Exemples de bibliothèques PHP pour SVN :

* intégré dans PHP lui-même : http://php.net/manual/en/book.svn.php

## Liste des indicateurs

[Indicateurs](indicateurs.md)