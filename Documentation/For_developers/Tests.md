# Tests de robustesse:

## Liens défectueux (un lien qui ne marchera jamais car il ne point pas vers un repository du genre www.google.com )

* tester avec un lien .git defectueux >> retourne des stats à zero sans crash
* tester avec un lien svn defectueux >> retourne des stats à zero sans crash
* tester avec un lien .git defectueux après un telechargement des fichiers ulterieur >> retourne des stats à zero sans crash
* tester avec un lient svn defectueux mais un nom de projet qui a déjà été telechargé >> retourne les stats de la derniere fois.

## 
