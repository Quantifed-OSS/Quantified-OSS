# Use cases

## Calcul des indicateurs : cas général

1. `git clone` ou `svn co`
1. calcul effectif des valeurs brutes et valeurs finales
1. stockage des valeurs en base
1. dans la base, suppression des valeurs précédentes des indicateurs pour ce repos.
1. suppression de la copie locale du repos

## Calcul des indicateurs : cas repos *présent* dans List Manager

1. Si copie locale n'existe pas : `git clone` ou `svn co`
   Sinon `git pull` ou `svn update`
1. calcul effectif des valeurs brutes et valeurs finales
1. stockage des valeurs en base
1. suppression des valeurs précédentes dans la base

Note: la copie locale du repos est conservée pour accélérer les futures mises à jour

## Consommateur de l'API

L'API de QOSS étant publique, plusieurs programmes peuvent en être consommateurs. Le Comptoir du Libre en est un. On peut aussi imaginer une application web permettant à un utilisateur de saisir l'URL d'un repos afin d'en obtenir les valeurs. Nous parlerons de Consommateur Comptoir et de Consommateur anonyme (une application web).

## Calcul initial des valeurs des indicateurs

Si la base de données est vide au démarrage de l'application QOSS, le calcul initial des valeurs des indicateurs doit être lancé.

Si la base de données n'est pas vide, le calcul des données d'un repos doit être fait lors de l'ajout dudit repos.

## Recalcul des valeurs des indicateurs

* Les valeurs des indicateurs sont recalculées à intervalle de temps régulier : tous les mois.
* Les repos concernés par le recalcul sont :
    * ceux présents dans la Liste Comptoir (gérée par le List Manager)
    * ceux ajoutés par des consommateurs anonymes

## Spécificités pour le Consommateur Comptoir

Traitements particuliers pour les repos du Comptoir :

* Les copies locales des repos sont conservées sur le disque (i.e. elles ne sont pas supprimées). Ceci permet de gagner le temps de clonage.
* Pour réduire le temps de transfert réseau, ces repos devraient être mis à jour toutes les semaines (afin d'éviter un "engorgement" réseau lors du recalcul mensuel).

